#include <glow/demo/common.hpp>

#include <glow/color.hpp>
#include <glow/renderer/plain.hpp>
#include <glow/mesh/axes.hpp>
#include <glow/camera/spherical.hpp>

#include <geom/transform/perspective.hpp>
#include <geom/transform/translation.hpp>
#include <geom/transform/rotation.hpp>
#include <geom/transform/homogeneous.hpp>
#include <geom/utility.hpp>
#include <geom/algorithms/delaunay.hpp>

#include <random>

template <typename T>
struct plain_mesh
{
	plain_mesh (glow::span<T> data, GLenum mode)
		: data_(data)
		, mode_(mode)
	{ }

	static constexpr auto attrs = glow::interleaved<geom::point_3f>();

	auto data ( ) const
	{
		return data_;
	}

	auto position_attr ( ) const
	{
		return std::get<0>(attrs);
	}

	GLenum mode ( ) const
	{
		return mode_;
	}

private:
	glow::span<T> data_;
	GLenum mode_;
};

struct delaunay_2d_demo
	: basic_demo
{
	std::vector<geom::point_3f> points;
	std::vector<geom::point_3f> edges;
	std::vector<geom::point_3f> triangles;

	glow::spherical_camera camera;
	bool middle_button_down;

	glow::renderer::plain renderer;
	glow::drawable points_drawable;
	glow::drawable edges_drawable;

	delaunay_2d_demo ( )
	{
		camera.fov_x = geom::pi_f / 3.f;
		camera.near_clip = 0.1f;
		camera.far_clip = 100.f;
		camera.azimuthal_angle = 0.f;
		camera.elevation_angle = geom::pi_f / 2.f;
		camera.distance = 5.f;

		gl::PointSize(5.f);
		gl::LineWidth(2.f);
//		gl::Enable(gl::DEPTH_TEST);

		glow::clear_color(glow::light(glow::gray));

		middle_button_down = false;

		std::default_random_engine rng{std::random_device{}()};

		std::uniform_real_distribution<float> d_phi(0.f, 2.f * geom::pi_f);
		std::normal_distribution<float> d_r(2.f, 0.4f);

		for (int i = 0; i < 10000; ++i)
		{
			float const phi = d_phi(rng);
			float r = d_r(rng);

			geom::vector_3f v {r * std::cos(phi), 0.f, r * std::sin(phi)};

			points.push_back(geom::point_3f::zero() + v);
		}

		std::vector<geom::point_2d> plane_points;
		for (auto p : points)
			plane_points.push_back({p[0], p[2]});

		auto triangle_idx = geom::delaunay(plane_points);

		for (std::size_t i = 0; i < triangle_idx.size(); ++i)
		{
			triangles.push_back(points[triangle_idx[i][0]]);
			triangles.push_back(points[triangle_idx[i][1]]);
			triangles.push_back(points[triangle_idx[i][2]]);
		}

		for (std::size_t i = 0; i < triangles.size(); i += 3)
		{
			edges.push_back(triangles[i + 0]);
			edges.push_back(triangles[i + 1]);
			edges.push_back(triangles[i + 1]);
			edges.push_back(triangles[i + 2]);
			edges.push_back(triangles[i + 2]);
			edges.push_back(triangles[i + 0]);
		}

		points_drawable = renderer.create(plain_mesh<geom::point_3f>{points, gl::POINTS});
		edges_drawable = renderer.create(plain_mesh<geom::point_3f>{edges, gl::LINES});

	}

	void resize (int width, int height) override
	{
		camera.aspect_ratio = static_cast<float>(width) / height;

		renderer.transform(camera.transform());
	}

	void mouse_button_middle (bool down) override
	{
		middle_button_down = down;
	}

	void mouse_move (int x, int y, int dx, int dy) override
	{
		if (middle_button_down)
		{
			camera.azimuthal_angle += dx * 0.01f;
			camera.elevation_angle += dy * 0.01f;

			renderer.transform(camera.transform());
		}
	}

	void mouse_wheel (int delta) override
	{
		camera.distance *= std::pow(0.8f, delta);
		renderer.transform(camera.transform());
	}

	void draw ( ) override
	{
		gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

		renderer.color(glow::dark(glow::blue));
		renderer.render(edges_drawable);

		renderer.color(glow::white);
		renderer.render(points_drawable);
	}
};

int main ( )
{
	run_demo<delaunay_2d_demo>();
}
