#include <glow/demo/common.hpp>

#include <glow/camera/spherical.hpp>
#include <glow/color.hpp>
#include <glow/object/buffer.hpp>
#include <glow/object/vertex_array.hpp>
#include <glow/object/program.hpp>
#include <glow/object/texture.hpp>
#include <glow/object/framebuffer.hpp>
#include <glow/drawable.hpp>
#include <glow/error.hpp>

#include <geom/utility.hpp>
#include <geom/operations/vector.hpp>
#include <geom/operations/matrix.hpp>

#include <util/clock.hpp>

geom::matrix_4x4f look_at (geom::point_3f const & position, geom::vector_3f const & direction, geom::vector_3f up)
{
	up = geom::normalized(up - geom::projection(up, direction));

	geom::vector_3f const right = direction ^ up;

	geom::matrix_4x4f m = geom::matrix_4x4f::zero();

	m[0][0] = right[0];
	m[1][0] = right[1];
	m[2][0] = right[2];

	m[0][1] = up[0];
	m[1][1] = up[1];
	m[2][1] = up[2];

	m[0][2] = -direction[0];
	m[1][2] = -direction[1];
	m[2][2] = -direction[2];

	m[0][3] = position[0];
	m[1][3] = position[1];
	m[2][3] = position[2];

	m[3][3] = 1.f;

	return geom::inverse(m);
}

geom::matrix_4x4f look_at (geom::point_3f const & position, geom::point_3f const & target, geom::vector_3f const & up)
{
	return look_at(position, geom::normalized(target - position), up);
}

struct shadow_mapping_demo
	: basic_demo
{

	static constexpr char depth_vs[] =
R"(#version 330

uniform mat4 u_projection;
uniform mat4 u_view;

layout (location = 0) in vec4 in_position;

void main ( )
{
	gl_Position = u_projection * u_view * in_position;
}

)";

	static constexpr char depth_fs[] =
R"(#version 330

void main ( )
{
	// do nothing, explicitly, successfully
}
)";

	static constexpr char colored_vs[] =
R"(#version 330

uniform mat4 u_projection;
uniform mat4 u_view;

layout (location = 0) in vec4 in_position;

void main ( )
{
	gl_Position = u_projection * u_view * in_position;
}

)";

	static constexpr char colored_fs[] =
R"(#version 330

uniform vec4 u_color;

void main ( )
{
	gl_FragColor = u_color;
}
)";

	static constexpr char textured_vs[] =
R"(#version 330

uniform mat4 u_projection;
uniform mat4 u_view;

layout (location = 0) in vec4 in_position;
layout (location = 1) in vec2 in_texcoord;

out vec2 texcoord;

void main ( )
{
	gl_Position = u_projection * u_view * in_position;
	texcoord = in_texcoord;
}

)";

	static constexpr char textured_fs[] =
R"(#version 330

uniform sampler2D u_texture;

in vec2 texcoord;

void main ( )
{
	gl_FragColor = texture(u_texture, texcoord);
}
)";

	static constexpr char shaded_vs[] =
R"(#version 330

uniform mat4 u_projection;
uniform mat4 u_view;
uniform mat4 u_light_projection_view;

layout (location = 0) in vec4 in_position;
layout (location = 1) in vec3 in_normal;

out vec3 position;
flat out vec3 normal;
out vec4 lightspace_position;

void main ( )
{
	gl_Position = u_projection * u_view * in_position;
	position = in_position.xyz;
	normal = in_normal;
	lightspace_position = u_light_projection_view * in_position;
}

)";

	static constexpr char shaded_fs[] =
R"(#version 330

uniform vec4 u_color;
uniform vec3 u_light_pos;
uniform sampler2D u_depth_texture;

in vec3 position;
flat in vec3 normal;
in vec4 lightspace_position;

void main ( )
{
	float illumination = dot(normalize(u_light_pos - position), normalize(normal));

	if (illumination < 0.0)
	{
		illumination = 0.2;
	}
	else
	{
		illumination = max(0.2, illumination);

		vec3 shadow_coord = lightspace_position.xyz / lightspace_position.w;


			if (texture(u_depth_texture, shadow_coord.xy * 0.5 + vec2(0.5, 0.5)).r < shadow_coord.z * 0.5 + 0.5)
				illumination = min(0.2, illumination);
	}

	gl_FragColor = mix(vec4(0.0, 0.0, 0.0, 1.0), u_color, illumination);
}
)";

	static constexpr float scene_size = 10.f;
	static constexpr float scene_height = 4.f;
	static constexpr float cylinder_radius = 1.f;

	static constexpr int shadow_map_resolution = 1024;

	glow::spherical_camera camera;
	bool middle_button_down;
	bool rotate_light;
	float light_angle;

	geom::vector_2i screen_size;

	glow::drawable light_source;
	glow::drawable scene;
	glow::drawable frustum;
	glow::drawable debug_quad;

	glow::texture light_depth_texture;
	glow::framebuffer light_depth_framebuffer;

	glow::program depth_program;
	glow::program colored_program;
	glow::program textured_program;
	glow::program shaded_program;

	util::clock<std::chrono::duration<float>> clock;

	shadow_mapping_demo ( )
		: middle_button_down { false }
		, rotate_light { true }
		, light_angle { 0.f }
		, light_source { make_light_source() }
		, scene { make_scene() }
		, frustum { make_frustum() }
		, debug_quad { make_debug_quad() }
		, depth_program { glow::vertex_shader(depth_vs), glow::fragment_shader(depth_fs) }
		, colored_program { glow::vertex_shader(colored_vs), glow::fragment_shader(colored_fs) }
		, textured_program { glow::vertex_shader(textured_vs), glow::fragment_shader(textured_fs) }
		, shaded_program { glow::vertex_shader(shaded_vs), glow::fragment_shader(shaded_fs) }
	{
		camera.fov_x = geom::pi_f / 3.f;
		camera.near_clip = 1.0f;
		camera.far_clip = 200.f;
		camera.azimuthal_angle = -geom::pi_f / 4.f;
		camera.elevation_angle = geom::pi_f / 6.f;
		camera.distance = 30.f;

		glow::clear_color(glow::dark(glow::blue));

		light_depth_texture.bind_scoped(gl::TEXTURE_2D)
			.parameter(gl::TEXTURE_MIN_FILTER, gl::NEAREST)
			.parameter(gl::TEXTURE_MAG_FILTER, gl::NEAREST)
			.parameter(gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE)
			.parameter(gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE)
			.image_2d(0, gl::DEPTH_COMPONENT16, {shadow_map_resolution, shadow_map_resolution}, gl::DEPTH_COMPONENT, gl::FLOAT, nullptr);

		light_depth_framebuffer.bind_scoped(gl::FRAMEBUFFER)
			.attach_depth(light_depth_texture)
			.assert_complete();
	}

	void resize (int width, int height) override
	{
		camera.aspect_ratio = static_cast<float>(width) / height;
		screen_size = { width, height };
	}

	void mouse_button_middle (bool down) override
	{
		middle_button_down = down;
	}

	void mouse_button_left (bool down) override
	{
		if (down)
			rotate_light = !rotate_light;
	}

	void mouse_move (int x, int y, int dx, int dy) override
	{
		if (middle_button_down)
		{
			camera.azimuthal_angle += dx * 0.01f;
			camera.elevation_angle += dy * 0.01f;
		}
	}

	void mouse_wheel (int delta) override
	{
		camera.distance *= std::pow(0.8f, delta);
	}

	static glow::drawable make_light_source ( )
	{
		std::vector<geom::point_3f> vertices;

		vertices.push_back({-1.f,  0.f,  0.f});
		vertices.push_back({ 1.f,  0.f,  0.f});
		vertices.push_back({ 0.f, -1.f,  0.f});
		vertices.push_back({ 0.f,  1.f,  0.f});
		vertices.push_back({ 0.f,  0.f, -1.f});
		vertices.push_back({ 0.f,  0.f,  1.f});

		glow::drawable d;
		d.mode = gl::LINES;
		d.count = vertices.size();
		d.vbo = glow::buffer();
		d.vao = glow::vertex_array();
		d.vbo.bind_scoped(gl::ARRAY_BUFFER)
			.data(vertices)
			.with([&]{
				auto desc = glow::interleaved<geom::point_3f>();
				d.vao.bind_scoped()
					.attrib(0, std::get<0>(desc))
					;
			});

		return d;
	}

	static glow::drawable make_frustum ( )
	{
		std::vector<geom::point_3f> vertices;

		vertices.push_back({-1.f, -1.f, -1.f});
		vertices.push_back({ 1.f, -1.f, -1.f});
		vertices.push_back({-1.f,  1.f, -1.f});
		vertices.push_back({ 1.f,  1.f, -1.f});
		vertices.push_back({-1.f, -1.f,  1.f});
		vertices.push_back({ 1.f, -1.f,  1.f});
		vertices.push_back({-1.f,  1.f,  1.f});
		vertices.push_back({ 1.f,  1.f,  1.f});

		vertices.push_back({-1.f, -1.f, -1.f});
		vertices.push_back({-1.f,  1.f, -1.f});
		vertices.push_back({ 1.f, -1.f, -1.f});
		vertices.push_back({ 1.f,  1.f, -1.f});
		vertices.push_back({-1.f, -1.f,  1.f});
		vertices.push_back({-1.f,  1.f,  1.f});
		vertices.push_back({ 1.f, -1.f,  1.f});
		vertices.push_back({ 1.f,  1.f,  1.f});

		vertices.push_back({-1.f, -1.f, -1.f});
		vertices.push_back({-1.f, -1.f,  1.f});
		vertices.push_back({ 1.f, -1.f, -1.f});
		vertices.push_back({ 1.f, -1.f,  1.f});
		vertices.push_back({-1.f,  1.f, -1.f});
		vertices.push_back({-1.f,  1.f,  1.f});
		vertices.push_back({ 1.f,  1.f, -1.f});
		vertices.push_back({ 1.f,  1.f,  1.f});

		glow::drawable d;
		d.mode = gl::LINES;
		d.count = vertices.size();
		d.vbo = glow::buffer();
		d.vao = glow::vertex_array();
		d.vbo.bind_scoped(gl::ARRAY_BUFFER)
			.data(vertices)
			.with([&]{
				auto desc = glow::interleaved<geom::point_3f>();
				d.vao.bind_scoped()
					.attrib(0, std::get<0>(desc))
					;
			});

		return d;
	}

	glow::drawable make_debug_quad ( )
	{
		struct vertex
		{
			geom::point_2f position;
			geom::vector_2f texcoord;
		};

		std::vector<vertex> vertices;

		vertices.push_back({{-1.f,  -1.f }, {0.f, 0.f}});
		vertices.push_back({{-0.5f, -1.f }, {1.f, 0.f}});
		vertices.push_back({{-0.5f, -0.5f}, {1.f, 1.f}});

		vertices.push_back({{-1.f,  -1.f }, {0.f, 0.f}});
		vertices.push_back({{-0.5f, -0.5f}, {1.f, 1.f}});
		vertices.push_back({{-1.f,  -0.5f}, {0.f, 1.f}});

		glow::drawable d;
		d.mode = gl::TRIANGLES;
		d.count = vertices.size();
		d.vbo = glow::buffer();
		d.vao = glow::vertex_array();
		d.vbo.bind_scoped(gl::ARRAY_BUFFER)
			.data(vertices)
			.with([&]{
				auto desc = glow::interleaved<geom::point_2f, geom::vector_2f>();
				d.vao.bind_scoped()
					.attrib(0, std::get<0>(desc))
					.attrib(1, std::get<1>(desc))
					;
			});

		return d;
	}

	static glow::drawable make_scene ( )
	{
		struct vertex
		{
			geom::point_3f position;
			geom::vector_3f normal;
		};

		std::vector<vertex> vertices;

		constexpr float plane_width = scene_size;
		constexpr float cylinder_height = scene_height;

		// plane quad
		vertices.push_back({{-plane_width, 0.f, -plane_width}, {0.f, 1.f, 0.f}});
		vertices.push_back({{ plane_width, 0.f,  plane_width}, {0.f, 1.f, 0.f}});
		vertices.push_back({{ plane_width, 0.f, -plane_width}, {0.f, 1.f, 0.f}});

		vertices.push_back({{-plane_width, 0.f, -plane_width}, {0.f, 1.f, 0.f}});
		vertices.push_back({{-plane_width, 0.f,  plane_width}, {0.f, 1.f, 0.f}});
		vertices.push_back({{ plane_width, 0.f,  plane_width}, {0.f, 1.f, 0.f}});

		// cylinders
		std::vector<std::size_t> sides {3, 4, 6, 8, 12};
		for (std::size_t s = 0; s < sides.size(); ++s)
		{
			geom::point_3f base
			{
				(-1.f + 2.f * (s + 0.5f) / sides.size()) * scene_size,
				0.f,
				0.f
			};

			// wall

			auto push_wall = [&](float a, float h, float an)
			{
				vertices.push_back({base + geom::vector_3f{
						std::cos(a) * cylinder_radius,
						h * cylinder_height,
						std::sin(a) * cylinder_radius
					}, geom::vector_3f{
						std::cos(an),
						0.f,
						std::sin(an)
					}});
			};

			std::size_t n = sides[s];

			for (std::size_t i = 0; i < n; ++i)
			{
				float const a0 = (geom::pi_f * 2.f / n) * (i + 0);
				float const a1 = (geom::pi_f * 2.f / n) * (i + 1);
				float const an = (a0 + a1) * 0.5f;

				push_wall(a0, 0.f, an);
				push_wall(a1, 1.f, an);
				push_wall(a1, 0.f, an);

				push_wall(a0, 0.f, an);
				push_wall(a0, 1.f, an);
				push_wall(a1, 1.f, an);
			}

			// cap

			auto push_cap = [&](float a, float r = 1.f)
			{
				vertices.push_back({base + geom::vector_3f{
						std::cos(a) * cylinder_radius * r,
						cylinder_height,
						std::sin(a) * cylinder_radius * r
					}, geom::vector_3f{
						0.f,
						1.f,
						0.f
					}});
			};

			for (std::size_t i = 0; i < n; ++i)
			{
				float const a0 = (geom::pi_f * 2.f / n) * (i + 0);
				float const a1 = (geom::pi_f * 2.f / n) * (i + 1);

				push_cap(0.f, 0.f);
				push_cap(a1);
				push_cap(a0);
			}
		}

		glow::drawable d;
		d.mode = gl::TRIANGLES;
		d.count = vertices.size();
		d.vbo = glow::buffer();
		d.vao = glow::vertex_array();
		d.vbo.bind_scoped(gl::ARRAY_BUFFER)
			.data(vertices)
			.with([&]{
				auto desc = glow::interleaved<geom::point_3f, geom::vector_3f>();
				d.vao.bind_scoped()
					.attrib(0, std::get<0>(desc))
					.attrib(1, std::get<1>(desc))
					;
			});

		return d;
	}

	void draw ( ) override
	{
		constexpr float light_radius = 30.f;
		constexpr float light_angular_speed = 0.3f;

		if (rotate_light)
			light_angle += clock.count() * light_angular_speed;
		clock.restart();

		geom::point_3f light_pos
		{
			std::cos(light_angle) * light_radius,
			30.f,
			std::sin(light_angle) * light_radius,
		};

		auto light_model_matrix = geom::translation<float, 3>(light_pos - geom::point_3f::zero()).homogeneous_matrix();

		gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

		geom::point_3f light_target { 0.f, 0.f, 0.f };
		geom::vector_3f light_up { 0.f, 1.f, 0.f };

		auto light_view_matrix = look_at(light_pos, light_target, light_up);

		geom::matrix_4x4f light_projection_matrix;

		{
			float light_projection_far = std::numeric_limits<float>::infinity();
			float light_projection_near = -std::numeric_limits<float>::infinity();
			float light_projection_left = std::numeric_limits<float>::infinity();
			float light_projection_right = -std::numeric_limits<float>::infinity();
			float light_projection_bottom = std::numeric_limits<float>::infinity();
			float light_projection_top = -std::numeric_limits<float>::infinity();

			std::vector<geom::point_3f> scene_bbox_vertices;
			scene_bbox_vertices.push_back({-scene_size, 0.f, -scene_size});
			scene_bbox_vertices.push_back({ scene_size, 0.f, -scene_size});
			scene_bbox_vertices.push_back({-scene_size, 0.f,  scene_size});
			scene_bbox_vertices.push_back({ scene_size, 0.f,  scene_size});
			scene_bbox_vertices.push_back({-scene_size, scene_height, -scene_size});
			scene_bbox_vertices.push_back({ scene_size, scene_height, -scene_size});
			scene_bbox_vertices.push_back({-scene_size, scene_height,  scene_size});
			scene_bbox_vertices.push_back({ scene_size, scene_height,  scene_size});

			for (geom::point_3f const & p : scene_bbox_vertices)
			{
				auto q = light_view_matrix * geom::homogeneous(p);

				geom::make_min(light_projection_left, -q[0] / q[2]);
				geom::make_max(light_projection_right, -q[0] / q[2]);

				geom::make_min(light_projection_bottom, -q[1] / q[2]);
				geom::make_max(light_projection_top, -q[1] / q[2]);

				geom::make_min(light_projection_far, q[2]);
				geom::make_max(light_projection_near, q[2]);
			}

			light_projection_matrix = geom::perspective<float, 3>(
					- light_projection_near * light_projection_left,
					- light_projection_near * light_projection_right,
					- light_projection_near * light_projection_bottom,
					- light_projection_near * light_projection_top,
					-light_projection_near,
					-light_projection_far
				).homogeneous_matrix();
		}

		auto light_projection_view_inverse_matrix = geom::inverse(light_projection_matrix * light_view_matrix);

		gl::Enable(gl::DEPTH_TEST);
		gl::Enable(gl::CULL_FACE);

		gl::DrawBuffer(gl::NONE);

		gl::CullFace(gl::FRONT);

		light_depth_framebuffer.bind_scoped(gl::FRAMEBUFFER)
			.with([&]{
				gl::Viewport(0, 0, shadow_map_resolution, shadow_map_resolution);

				gl::Clear(gl::DEPTH_BUFFER_BIT);

				depth_program.bind_scoped()
					.uniform("u_projection", light_projection_matrix)
					.uniform("u_view", light_view_matrix)
					.with([&]{
						scene.draw();
					});
			});

		gl::DrawBuffer(gl::BACK);
		gl::CullFace(gl::BACK);

		gl::Viewport(0, 0, screen_size[0], screen_size[1]);

		colored_program.bind_scoped()
			.uniform("u_projection", camera.projection())
			.uniform("u_view", camera.view() * light_model_matrix)
			.uniform("u_color", glow::yellow)
			.with([&]{
				light_source.draw();
			})
			.uniform("u_projection", camera.projection())
			.uniform("u_view", camera.view() * light_projection_view_inverse_matrix)
			.uniform("u_color", glow::red)
			.with([&]{
				frustum.draw();
			})
			;

		shaded_program.bind_scoped()
			.uniform("u_projection", camera.projection())
			.uniform("u_view", camera.view())
			.uniform("u_light_projection_view", light_projection_matrix * light_view_matrix)
			.uniform("u_color", glow::white)
			.uniform("u_light_pos", light_pos)
			.uniform("u_depth_texture", 0)
			.with([&]{
				light_depth_texture.bind_scoped(gl::TEXTURE_2D)
					.with([&]{
						scene.draw();
					});
			});

		gl::Disable(gl::DEPTH_TEST);
		gl::Disable(gl::CULL_FACE);

		textured_program.bind_scoped()
			.uniform("u_projection", geom::matrix_4x4f::identity())
			.uniform("u_view", geom::matrix_4x4f::identity())
			.uniform("u_texture", 0)
			.with([&]{
				light_depth_texture.bind_scoped(gl::TEXTURE_2D)
					.with([&]{
						debug_quad.draw();
					});
			});
	}
};

int main ( )
{
	run_demo<shadow_mapping_demo>();
}
