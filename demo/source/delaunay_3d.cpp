#include <glow/demo/common.hpp>

#include <glow/color.hpp>
#include <glow/renderer/plain.hpp>
#include <glow/mesh/axes.hpp>
#include <glow/camera/spherical.hpp>

#include <geom/utility.hpp>
#include <geom/algorithms/delaunay_3d.hpp>

#include <util/clock.hpp>

#include <random>
#include <array>
#include <map>
#include <iostream>
#include <list>

template <typename T>
struct plain_mesh
{
	plain_mesh (glow::span<T> data, GLenum mode)
		: data_(data)
		, mode_(mode)
	{ }

	static constexpr auto attrs = glow::interleaved<geom::point_3f>();

	auto data ( ) const
	{
		return data_;
	}

	auto position_attr ( ) const
	{
		return std::get<0>(attrs);
	}

	GLenum mode ( ) const
	{
		return mode_;
	}

private:
	glow::span<T> data_;
	GLenum mode_;
};

template <typename T>
plain_mesh (std::vector<T> const &, GLenum) -> plain_mesh<T>;

struct delaunay_3d_demo
	: basic_demo
{
	std::vector<geom::point_3f> points;
	std::vector<geom::vector_3f> velocities;

	glow::spherical_camera camera;
	bool middle_button_down;
	bool paused;

	glow::renderer::plain renderer;
	glow::drawable points_drawable;
	glow::drawable edges_drawable;
	glow::drawable triangles_drawable;

	delaunay_3d_demo ( )
	{
		camera.fov_x = geom::pi_f / 3.f;
		camera.near_clip = 0.1f;
		camera.far_clip = 100.f;
		camera.azimuthal_angle = -geom::pi_f / 4.f;
		camera.elevation_angle = geom::pi_f / 6.f;
		camera.distance = 5.f;

		gl::PointSize(5.f);
		gl::LineWidth(2.f);
		gl::Enable(gl::DEPTH_TEST);
		gl::DepthFunc(gl::LEQUAL);

		glow::clear_color(glow::black);

		middle_button_down = false;
		paused = true;

		std::default_random_engine rng{std::random_device{}()};

		std::normal_distribution<float> d;

		for (int i = 0; i < 40; ++i)
		{
			geom::point_3f p {d(rng), d(rng), d(rng)};
			geom::vector_3f v {d(rng), d(rng), d(rng)};
			points.push_back(p);
			velocities.push_back(v);
		}
	}

	void mouse_button_left (bool down)
	{
		if (down)
			paused = !paused;
	}

	void resize (int width, int height) override
	{
		camera.aspect_ratio = static_cast<float>(width) / height;

		renderer.transform(camera.transform());
	}

	void mouse_button_middle (bool down) override
	{
		middle_button_down = down;
	}

	void mouse_move (int x, int y, int dx, int dy) override
	{
		if (middle_button_down)
		{
			camera.azimuthal_angle += dx * 0.01f;
			camera.elevation_angle += dy * 0.01f;

			renderer.transform(camera.transform());
		}
	}

	void mouse_wheel (int delta) override
	{
		camera.distance *= std::pow(0.8f, delta);
		renderer.transform(camera.transform());
	}

	void draw ( ) override
	{
		if (!paused)
		{
			float const dt = 0.001f;

			for (std::size_t i = 0; i < points.size(); ++i)
			{
				static const auto o = geom::point_3f::zero();

				velocities[i] += (o - points[i]) * dt;
				points[i] += velocities[i] * dt;
			}
		}

		util::clock<std::chrono::duration<double>, std::chrono::high_resolution_clock> timer;
		auto tetrahedra_idx = geom::delaunay(points);
		std::cout << "Delaunay Took " << timer.count() << " seconds\n";
		std::cout << tetrahedra_idx.size() << " tetrahedra\n";

		std::vector<geom::point_3f> edges;
		std::vector<geom::point_3f> triangles;

		for (auto const & t : tetrahedra_idx)
		{
//			for (std::size_t i : {0, 1, 2, 3})
//			{
//				for (std::size_t j = i + 1; j < 4; ++j)
//				{
//					edges.push_back(points[t[i]]);
//					edges.push_back(points[t[j]]);
//				}
//			}

			auto const p0 = points[t[0]];
			auto const p1 = points[t[1]];
			auto const p2 = points[t[2]];
			auto const p3 = points[t[3]];

			auto const c = p0 + (p1 - p0) / 4.f + (p2 - p0) / 4.f + (p3 - p0) / 4.f;

			float const s = 0.75f;

			auto const q0 = c + (p0 - c) * s;
			auto const q1 = c + (p1 - c) * s;
			auto const q2 = c + (p2 - c) * s;
			auto const q3 = c + (p3 - c) * s;

			triangles.push_back(q0);
			triangles.push_back(q1);
			triangles.push_back(q2);

			triangles.push_back(q0);
			triangles.push_back(q1);
			triangles.push_back(q3);

			triangles.push_back(q0);
			triangles.push_back(q2);
			triangles.push_back(q3);

			triangles.push_back(q1);
			triangles.push_back(q2);
			triangles.push_back(q3);

			edges.push_back(q0);
			edges.push_back(q1);

			edges.push_back(q0);
			edges.push_back(q2);

			edges.push_back(q0);
			edges.push_back(q3);

			edges.push_back(q1);
			edges.push_back(q2);

			edges.push_back(q1);
			edges.push_back(q3);

			edges.push_back(q2);
			edges.push_back(q3);
		}

		gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

		renderer.update(triangles_drawable, plain_mesh{triangles, gl::TRIANGLES});
		renderer.color(glow::dark(glow::blue));
		renderer.render(triangles_drawable);

		renderer.update(edges_drawable, plain_mesh{edges, gl::LINES});
		renderer.color(glow::yellow);
		renderer.render(edges_drawable);

		renderer.update(points_drawable, plain_mesh{points, gl::POINTS});
		renderer.color(glow::white);
		renderer.render(points_drawable);
	}
};

int main ( )
{
	run_demo<delaunay_3d_demo>();
}
