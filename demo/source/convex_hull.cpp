#include <glow/context.hpp>
#include <glow/object/program.hpp>
#include <glow/object/buffer.hpp>
#include <glow/object/vertex_array.hpp>
#include <glow/color.hpp>

#include <geom/transform/scale.hpp>
#include <geom/transform/homogeneous.hpp>
#include <geom/random/point.hpp>
#include <geom/random/vector.hpp>
#include <geom/algorithms/convex_hull/quickhull.hpp>
#include <geom/utility.hpp>

#include <glow/demo/common.hpp>

#include <chrono>

static float pow5 (float x)
{
	auto x2 = x * x;
	return x2 * x2 * x;
}

struct convex_hull_demo
	: basic_demo
{
	static constexpr char vs[] =
R"(#version 330

uniform mat4 u_transform;

layout (location = 0) in vec4 in_position;

void main ( )
{
	gl_Position = u_transform * in_position;
}
)";

	static constexpr char fs[] =
R"(#version 330

uniform vec4 u_color;

void main ( )
{
	gl_FragColor = u_color;
}
)";

	static constexpr float view_size = 15.f;

	static constexpr std::size_t particle_count = 10000;

	static constexpr float dt = 0.003f;

	static float wall (geom::point_2f p)
	{
		static constexpr float r1 = 10.f;
		static constexpr float r2 = 7.f;

		using namespace std;

		return pow5(r1) - (pow5(abs(p[0])) + pow5(abs(p[1])));
	}

	static geom::vector_2f wall_grad (geom::point_2f p, float v)
	{
		static constexpr float eps = 1e-6;

		auto vx = wall(p + geom::vector_2f{eps, 0.f});
		auto vy = wall(p + geom::vector_2f{0.f, eps});

		return geom::vector_2f{vx - v, vy - v} / eps;
	}

	glow::program program;

	std::vector<geom::point_2f> particle_pos;
	std::vector<geom::vector_2f> particle_vel;

	glow::buffer particle_vbo;
	glow::vertex_array particle_vao;

	glow::buffer convex_hull_vbo;
	glow::vertex_array convex_hull_vao;
	GLuint convex_hull_count;

	glow::buffer obb_vbo;
	glow::vertex_array obb_vao;

	convex_hull_demo ( )
		: program { glow::vertex_shader(vs), glow::fragment_shader(fs) }
	{
		gl::ClearColor(0.f, 0.f, 0.1f, 1.f);
		gl::LineWidth(5.f);

		particle_vbo.bind_scoped(gl::ARRAY_BUFFER)
			.with([&]{
				particle_vao.bind_scoped()
					.attrib<geom::point_2f>(0, gl::FALSE_, 0, nullptr);
			});

		convex_hull_vbo.bind_scoped(gl::ARRAY_BUFFER)
			.with([&]{
				convex_hull_vao.bind_scoped()
					.attrib<geom::point_2f>(0, gl::FALSE_, 0, nullptr);
			});
		convex_hull_count = 0;

		obb_vbo.bind_scoped(gl::ARRAY_BUFFER)
			.with([&]{
				obb_vao.bind_scoped()
					.attrib<geom::point_2f>(0, gl::FALSE_, 0, nullptr);
			});

		std::default_random_engine rng(std::chrono::system_clock::now().time_since_epoch().count());

		geom::random::uniform_disk_point_distribution<geom::point_2f> random_pos {
			{0.f, 0.f},
			view_size
		};

		geom::random::uniform_sphere_vector_distribution<geom::vector_2f> random_vel;

		for (std::size_t i = 0; i < particle_count; ++i)
		{
			geom::point_2f pos;
			do
			{
				pos = random_pos(rng);
			}
			while (wall(pos) < 0.f);

			particle_pos.push_back(pos);
			particle_vel.push_back(random_vel(rng));
		}
	}

	void resize (int width, int height) override
	{
		float const ratio = static_cast<float>(width) / height;

		float const s = 1.f / view_size;

		gl::Viewport(0, 0, width, height);

		program.bind_scoped()
				.uniform("u_transform", geom::scale<float, 4>({s / ratio, s, 1.f, 1.f}).matrix())
			;
	}

	void step ( )
	{
		for (std::size_t i = 0; i < particle_count; ++i)
		{
			particle_pos[i] += particle_vel[i] * dt;

			auto const v = wall(particle_pos[i]);

			if (v < 0.f)
			{
				auto const g = wall_grad(particle_pos[i], v);

				particle_pos[i] -= v * g / (g * g);

				auto const n = geom::normalized(g);

				particle_vel[i] -= 2.f * n * (particle_vel[i] * n);
			}
		}

		particle_vbo.bind_scoped(gl::ARRAY_BUFFER)
			.data(particle_pos, gl::STREAM_DRAW);
	}

	void update_hull ( )
	{
		std::vector<geom::point_2f> particles_hull;
		particles_hull.reserve(particle_count);
		geom::quickhull(particle_pos.begin(), particle_pos.end(), std::back_inserter(particles_hull));

		float min_obb_area = std::numeric_limits<float>::infinity();
		geom::point_2f min_obb[4];
		for (std::size_t edge = 0; edge < particles_hull.size(); ++edge)
		{
			std::size_t const start = edge;
			std::size_t const end = (edge + 1) % particles_hull.size();
			geom::vector_2f const edge_v = particles_hull[end] - particles_hull[start];

			std::size_t left = start;
			std::size_t right = start;
			std::size_t bottom = start;

			auto const left_fn = [&](std::size_t p){
				return (particles_hull[p] - particles_hull[start]) * edge_v;
			};

			auto const right_fn = [&](std::size_t p){
				return -(particles_hull[p] - particles_hull[start]) * edge_v;
			};

			auto const bottom_fn = [&](std::size_t p){
				return geom::det(particles_hull[end] - particles_hull[start], particles_hull[p] - particles_hull[start]);
			};

			for (std::size_t p = 0; p < particles_hull.size(); ++p)
			{
				if (left_fn(p) > left_fn(left))
					left = p;

				if (right_fn(p) > right_fn(right))
					right = p;

				if (bottom_fn(p) > bottom_fn(bottom))
					bottom = p;
			}

			geom::point_2f obb[4];

			geom::vector_2f const edge_n = geom::normalized(particles_hull[(edge + 1) % particles_hull.size()] - particles_hull[edge]);
			geom::vector_2f const edge_no = geom::ort(edge_n);

			obb[0] = particles_hull[edge] + edge_n * (edge_n * (particles_hull[right] - particles_hull[edge]));
			obb[1] = particles_hull[edge] + edge_n * (edge_n * (particles_hull[left] - particles_hull[edge]));

			obb[3] = obb[0] + edge_no * (edge_no * (particles_hull[bottom] - particles_hull[edge]));
			obb[2] = obb[0] + (obb[1] - obb[0]) + (obb[3] - obb[0]);

			float const area = geom::norm(obb[1] - obb[0]) * geom::norm(obb[3] - obb[0]);

			if (area < min_obb_area)
			{
				min_obb_area = area;
				std::copy(obb, obb + 4, min_obb);
			}
		}

		convex_hull_vbo.bind_scoped(gl::ARRAY_BUFFER)
			.data(particles_hull, gl::STREAM_DRAW);
		convex_hull_count = particles_hull.size();

		obb_vbo.bind_scoped(gl::ARRAY_BUFFER)
			.data(min_obb, gl::STREAM_DRAW);
	}

	void draw ( ) override
	{
		step();
		update_hull();

		gl::Clear(gl::COLOR_BUFFER_BIT);

		program.bind_scoped()
			.uniform("u_color", glow::white)
			.with([&]{
				particle_vao.bind_scoped().draw(gl::POINTS, 0, particle_count);
			})
			.uniform("u_color", glow::green)
			.with([&]{
				convex_hull_vao.bind_scoped().draw(gl::LINE_LOOP, 0, convex_hull_count);
			})
			.uniform("u_color", glow::red)
			.with([&]{
//				obb_vao.bind_scoped().draw(gl::LINE_LOOP, 0, 4);
			});
	}
};

int main ( )
{
	run_demo<convex_hull_demo>();
}
