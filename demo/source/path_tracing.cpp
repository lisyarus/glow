#include <glow/demo/common.hpp>

#include <glow/color.hpp>

#include <glow/drawable.hpp>
#include <glow/object/program.hpp>
#include <glow/object/framebuffer.hpp>

#include <iostream>

static float light_y = 10.f;

struct path_tracing_demo
	: basic_demo
{
	static constexpr char raytrace_vs[] =
R"(#version 330

layout (location = 0) in vec2 in_position;

out vec2 position;

void main ( )
{
	gl_Position = vec4(in_position, 0.0, 1.0);
	position = in_position;
}
)";

	static constexpr char raytrace_fs[] =
R"(#version 330

uniform vec2 frustum;
uniform int seed;
uniform sampler2D u_back_texture;
uniform int u_frame_number;
uniform float u_camera_a;
uniform float u_camera_b;
uniform vec3 u_camera_pos;

in vec2 position;

out vec4 out_color;

float rand (int i) {
	return fract(sin(dot(position.xy * cos(i * 0.0195378), vec2(12.9898, 78.233))) * 43758.5453);
}

struct ray
{
	vec3 origin;
	vec3 direction;
};

struct plane_eq
{
	vec4 func;
};

struct sphere_eq
{
	vec3 position;
	float radius;
};

struct material
{
	vec3 color;
	float emission;
	float reflectance;
	float fuzziness;
};

struct plane
{
	plane_eq eq;
	material mat;
};

struct sphere
{
	sphere_eq eq;
	material mat;
};

struct intersection
{
	float t;
	vec3 normal;
};

float intersect (ray r, plane_eq p)
{
	return - (dot(r.origin, p.func.xyz) + p.func.w) / dot(r.direction, p.func.xyz);
}

float intersect (ray r, sphere_eq s)
{
	float a = dot(r.direction, r.direction);
	float b_2 = dot(r.direction, r.origin - s.position);
	float c = dot(r.origin - s.position, r.origin - s.position) - s.radius * s.radius;

	float D = b_2*b_2 - a*c;

	if (D < 0.0)
		return -1.0;

	float sqrtD = sqrt(D);

	float t1 = (- b_2 - sqrtD) / a;
	float t2 = (- b_2 + sqrtD) / a;

	if (t1 > 0.0)
		return t1;

	return t2;
}

intersection intersect (ray r, plane p)
{
	intersection i;
	i.t = intersect(r, p.eq);
	i.normal = normalize(p.eq.func.xyz);
	return i;
}

intersection intersect (ray r, sphere s)
{
	intersection i;
	i.t = intersect(r, s.eq);
	i.normal = normalize(r.origin + i.t * r.direction - s.eq.position);
	return i;
}

plane planes[] = plane[](
	plane(plane_eq(vec4( 1.0, 0.0, 0.0, 10.0)), material(vec3(1.0, 0.0, 0.0), 0.0, 0.0, 0.0)),
	plane(plane_eq(vec4( 0.0, 0.0, 1.0, 10.0)), material(vec3(0.0, 1.0, 0.0), 0.0, 0.0, 0.0)),
	plane(plane_eq(vec4(-1.0, 0.0, 0.0, 10.0)), material(vec3(0.0, 0.0, 1.0), 0.0, 0.0, 0.0)),

	plane(plane_eq(vec4(0.0,  1.0, 0.0, 10.0)), material(vec3(1.0, 1.0, 1.0), 0.0, 0.0, 0.0)),
	plane(plane_eq(vec4(0.0, -1.0, 0.0, 10.0)), material(vec3(1.0, 1.0, 1.0), 1.0, 0.0, 0.0)),

	plane(plane_eq(vec4(0.0, 0.0, -1.0, 10.0)), material(vec3(1.0, 1.0, 1.0), 0.0, 0.0, 0.0))
);

sphere spheres[] = sphere[](
	sphere(sphere_eq(vec3(-5.0, -5.0, -5.0), 3.0), material(vec3(1.0, 1.0, 1.0), 0.0, 0.0, 0.0)),
	sphere(sphere_eq(vec3( 0.0, -8.0, 0.0), 1.5), material(vec3(1.0, 1.0, 1.0), 10000.0, 0.0, 0.0)),
	sphere(sphere_eq(vec3( 5.0, -5.0, -5.0), 3.0), material(vec3(1.0, 1.0, 1.0), 0.0, 0.0, 0.0))
);

intersection intersect_scene (ray r, out material mat)
{
	intersection i;
	i.t = -1.0;

	for (int j = 0; j < planes.length(); ++j)
	{
		intersection i1 = intersect(r, planes[j]);
		if (i1.t > 0.0 && (i.t < 0.0 || i1.t < i.t))
		{
			i = i1;
			mat = planes[j].mat;
		}
	}

	for (int j = 0; j < spheres.length(); ++j)
	{
		intersection i1 = intersect(r, spheres[j]);
		if (i1.t > 0.0 && (i.t < 0.0 || i1.t < i.t))
		{
			i = i1;
			mat = spheres[j].mat;
		}
	}

	return i;
}

vec3 raytrace (ray r, int k)
{
	const int bounce = 16;

	vec3 color = vec3(0.0, 0.0, 0.0);
	vec3 mask = vec3(1.0, 1.0, 1.0);

	for (int j = 0; j < bounce; ++j)
	{
		int rand_base = (k * bounce + j) * 3;

		intersection i;
		material mat;
		i = intersect_scene(r, mat);

		if (i.t < 0.0)
		{
			return color;
		}
		else
		{
			r.origin += i.t * r.direction;

			if (rand(rand_base + 1) < mat.reflectance)
			{
				vec3 reflection_dir = r.direction - 2.0 * i.normal * dot(r.direction, i.normal);

				vec3 u = normalize(cross((abs(reflection_dir.x) > 0.1 ? vec3(0, 1, 0) : vec3(1, 0, 0)), reflection_dir));
				vec3 v = cross(reflection_dir, u);

				float fuzzy_r = sqrt(rand(rand_base + 2)) * mat.fuzziness;
				float fuzzy_phi = rand(rand_base + 3) * 2.0 * 3.1415926535;

				reflection_dir = normalize(reflection_dir + (u * cos(fuzzy_phi) + v * sin(fuzzy_phi)) * fuzzy_r);

				r.direction = reflection_dir;
			}
			else
			{
				color += mask * mat.color * mat.emission;

				float r1 = 2 * 3.1415926535f * rand(rand_base + 2);
				float r2 = rand(rand_base + 3);
				float r2s = sqrt(r2);

				vec3 w = i.normal;
				vec3 u = normalize(cross((abs(w.x) > 0.1 ? vec3(0, 1, 0) : vec3(1, 0, 0)), w));
				vec3 v = cross(w, u);

				vec3 diffuse_dir = normalize(u*cos(r1)*r2s + v*sin(r1)*r2s + w*sqrt(1.0 - r2));

				r.direction = diffuse_dir;

				mask *= dot(diffuse_dir, i.normal);
			}

			mask *= mat.color;

			r.origin += r.direction * 0.01;
		}
	}

	return color;
}

vec4 gamma_correct (vec4 c)
{
	return sqrt(sqrt(c));
}

void main ( )
{
	vec3 camera_z = vec3(cos(u_camera_b) * sin(u_camera_a), sin(u_camera_b), cos(u_camera_b) * cos(u_camera_a));
	vec3 camera_x = vec3(cos(u_camera_a), 0.0, -sin(u_camera_a));
	vec3 camera_y = cross(camera_z, camera_x);

	ray r;
	r.origin = -u_camera_pos;
	r.direction = normalize(-camera_z + camera_x * position.x * frustum.x + camera_y * position.y * frustum.y);

	const int samples = 1;

	vec4 color = vec4(0.0, 0.0, 0.0, 0.0);

	for (int s = 0; s < samples; ++s)
	{
		color += gamma_correct(vec4(raytrace(r, seed), 1.0)) / float(samples);
	}

	vec2 texcoord = position * 0.5 + vec2(0.5, 0.5);
	out_color = mix(texture(u_back_texture, texcoord), color, 0.01);
	//out_color = texture(u_back_texture, texcoord) * (u_frame_number - 1.0) / u_frame_number + color / u_frame_number;
}
)";

	static constexpr char show_vs[] =
R"(#version 330

layout (location = 0) in vec2 in_position;

out vec2 texcoord;

void main ( )
{
	gl_Position = vec4(in_position, 0.0, 1.0);
	texcoord = in_position * 0.5 + vec2(0.5, 0.5);
}
)";

	static constexpr char show_fs[] =
R"(#version 330

uniform sampler2D u_texture;

in vec2 texcoord;

out vec4 out_color;

void main ( )
{
	out_color = textureLod(u_texture, texcoord, 10);
}
)";

	glow::drawable quad;
	glow::program raytrace_program;
	glow::program show_program;
	glow::texture back_texture;
	glow::framebuffer back_framebuffer;
	glow::texture front_texture;
	glow::framebuffer front_framebuffer;

	geom::vector_2f frustum;
	GLint frame_number;

	float camera_a, camera_b;
	geom::point_3f camera_pos;
	bool mouse_left_down;
	bool mouse_right_down;

	path_tracing_demo ( )
		: quad { fullscreen_quad() }
		, raytrace_program { glow::vertex_shader(raytrace_vs), glow::fragment_shader(raytrace_fs) }
		, show_program { glow::vertex_shader(show_vs), glow::fragment_shader(show_fs) }
		, frame_number { 0 }
		, camera_a { 0.8f }
		, camera_b { 0.45f }
		, camera_pos { -7.0f, 2.5f, -2.50f }
		, mouse_left_down { false }
		, mouse_right_down { false }
	{
		glow::clear_color(glow::black);
	}

	void resize (int width, int height) override
	{
		frustum[0] = static_cast<float>(width) / height;
		frustum[1] = 1.0f;

		back_texture.bind_scoped(gl::TEXTURE_2D)
			.parameter(gl::TEXTURE_MAG_FILTER, gl::NEAREST)
			.parameter(gl::TEXTURE_MIN_FILTER, gl::NEAREST)
			.image_2d<glow::color_3f>(0, {width, height}, nullptr);

		back_framebuffer.bind_scoped(gl::FRAMEBUFFER)
			.attach_color(back_texture)
			.assert_complete();

		front_texture.bind_scoped(gl::TEXTURE_2D)
			.parameter(gl::TEXTURE_MAG_FILTER, gl::NEAREST)
			.parameter(gl::TEXTURE_MIN_FILTER, gl::NEAREST)
			.image_2d<glow::color_3f>(0, {width, height}, nullptr);

		front_framebuffer.bind_scoped(gl::FRAMEBUFFER)
			.attach_color(front_texture)
			.assert_complete();
	}

	void draw ( ) override
	{
		float camera_move = 0.f;

		if( mouse_left_down )
		{
			camera_move += 0.1f;
		}

		if( mouse_right_down )
		{
			camera_move -= 0.1f;
		}

		camera_pos += geom::vector_3f {
			std::cos(camera_b) * std::sin(camera_a),
			std::sin(camera_b),
			std::cos(camera_b) * std::cos(camera_a)
		} * camera_move;

		++frame_number;

		for (int step = 0; step < 4; ++step)
		{
			front_framebuffer.bind_scoped(gl::FRAMEBUFFER)
				.with([&]{
					gl::Clear(gl::COLOR_BUFFER_BIT);

					raytrace_program.bind_scoped()
						.uniform("frustum", frustum)
						.uniform("seed", frame_number)
						.uniform("u_back_texture", 0)
						.uniform("u_frame_number", std::nothrow, frame_number)
						.uniform("u_camera_a", std::nothrow, camera_a)
						.uniform("u_camera_b", std::nothrow, camera_b)
						.uniform("u_camera_pos", std::nothrow, camera_pos)
						.with([&]{
							back_texture.bind_scoped(gl::TEXTURE_2D)
								.with([&]{ quad.draw(); });
						});
				});

			gl::Clear(gl::COLOR_BUFFER_BIT);

			show_program.bind_scoped()
				.uniform("u_texture", 0)
				.with([&]{
					front_texture.bind_scoped(gl::TEXTURE_2D)
						.generate_mipmap()
						.with([&]{
							quad.draw();
						});
				});

			std::swap(back_texture, front_texture);
			std::swap(back_framebuffer, front_framebuffer);
		}
	}

	bool first_mouse_move = true;

	void mouse_move (int x, int y, int dx, int dy)
	{
		if (first_mouse_move)
			first_mouse_move = false;
		else
		{
			camera_a -= dx * 0.01f;
			camera_b += dy * 0.01f;
		}
	}

	void mouse_button_left (bool down)
	{
		mouse_left_down = down;
	}

	void mouse_button_right (bool down)
	{
		mouse_right_down = down;
	}

	bool show_mouse ( ) const override
	{
		return false;
	}

	static glow::drawable fullscreen_quad ( )
	{
		std::vector<geom::point_2f> vertices;

		vertices.push_back({-1.f, -1.f});
		vertices.push_back({ 1.f, -1.f});
		vertices.push_back({-1.f,  1.f});
		vertices.push_back({ 1.f,  1.f});

		glow::drawable d;
		d.count = vertices.size();
		d.mode = gl::TRIANGLE_STRIP;
		d.vbo = glow::buffer();
		d.vao = glow::vertex_array();
		d.vbo.bind_scoped(gl::ARRAY_BUFFER)
			.data(vertices)
			.with([&]{
				d.vao.bind_scoped()
					.attrib<geom::point_2f>(0, gl::FALSE_, 0, nullptr);
			});

		return d;
	}

};

int main (int argc, char ** argv)
{
	if (argc >= 2)
	{
		light_y = 10.f - (std::atoi(argv[1]) * 0.001f) * 20.f;
	}
	run_demo<path_tracing_demo>();
}
