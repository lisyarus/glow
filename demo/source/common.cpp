#include <glow/demo/common.hpp>

#include <SDL2/SDL.h>

#include <glow/context.hpp>
#include <glow/error.hpp>

#include <util/clock.hpp>

#include <iostream>

void basic_demo::resize (int width, int height)
{
	gl::Viewport(0, 0, width, height);
}

void basic_demo::draw ( )
{
	gl::ClearColor(0.f, 0.f, 0.f, 0.f);
	gl::Clear(gl::COLOR_BUFFER_BIT);
}

void run_demo (std::function<std::unique_ptr<basic_demo>()> factory) try
{
	SDL_Init(SDL_INIT_VIDEO);

	SDL_Window * window = SDL_CreateWindow("GLOW", 0, 0, 800, 600, SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

	SDL_GLContext context = SDL_GL_CreateContext(window);

	SDL_GL_MakeCurrent(window, context);

	SDL_GL_SetSwapInterval(0);

	glow::load_functions();
	std::cout << "Successfully initialized OpenGL " << glow::context::version() << "\n";
	std::cout << "  Vendor   : " << glow::context::vendor() << '\n';
	std::cout << "  Renderer : " << glow::context::renderer() << '\n';

	std::unique_ptr<basic_demo> demo = factory();

	if (!demo->show_mouse())
	{
		SDL_ShowCursor(SDL_FALSE);
		SDL_SetRelativeMouseMode(SDL_TRUE);
	}

	util::clock<> clock;

	int frame_count = 0;
	std::chrono::seconds time_between_show_fps{1};

	for (bool running = true; running;)
	{
		for (SDL_Event event; SDL_PollEvent(&event);) switch (event.type)
		{
		case SDL_QUIT:
			running = false;
			break;
		case SDL_WINDOWEVENT:
			switch (event.window.event)
			{
			case SDL_WINDOWEVENT_RESIZED:
				demo->resize(event.window.data1, event.window.data2);
				break;
			}
			break;
		case SDL_MOUSEMOTION:
			demo->mouse_move(event.motion.x, event.motion.y, event.motion.xrel, event.motion.yrel);
			break;
		case SDL_MOUSEBUTTONDOWN:
			switch (event.button.button)
			{
			case SDL_BUTTON_LEFT:   demo->mouse_button_left  (true); break;
			case SDL_BUTTON_MIDDLE: demo->mouse_button_middle(true); break;
			case SDL_BUTTON_RIGHT:  demo->mouse_button_right (true); break;
			}
			break;
		case SDL_MOUSEBUTTONUP:
			switch (event.button.button)
			{
			case SDL_BUTTON_LEFT:   demo->mouse_button_left  (false); break;
			case SDL_BUTTON_MIDDLE: demo->mouse_button_middle(false); break;
			case SDL_BUTTON_RIGHT:  demo->mouse_button_right (false); break;
			}
			break;
		case SDL_MOUSEWHEEL:
			demo->mouse_wheel(event.wheel.direction == SDL_MOUSEWHEEL_NORMAL ? event.wheel.y : -event.wheel.y);
			break;
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				running = false;
				break;
			}
			break;
		}

		demo->draw();

		glow::error();

		SDL_GL_SwapWindow(window);

		++frame_count;

		if (clock.duration() >= time_between_show_fps)
		{
			std::cout << "FPS: " << (frame_count / clock.count()) << std::endl;
			clock.restart();
			frame_count = 0;
		}
	}
}
catch (std::exception const & e)
{
	std::cerr << "Error: " << e.what() << std::endl;
}
