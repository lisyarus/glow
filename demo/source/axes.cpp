#include <glow/demo/common.hpp>

#include <glow/color.hpp>
#include <glow/renderer/colored.hpp>
#include <glow/mesh/axes.hpp>
#include <glow/camera/spherical.hpp>

#include <geom/transform/perspective.hpp>
#include <geom/transform/translation.hpp>
#include <geom/transform/rotation.hpp>
#include <geom/transform/homogeneous.hpp>
#include <geom/utility.hpp>

struct axes_demo
	: basic_demo
{
	glow::spherical_camera camera;
	bool middle_button_down;

	glow::renderer::colored renderer;
	glow::drawable axes;

	axes_demo ( )
	{
		camera.fov_x = geom::pi_f / 3.f;
		camera.near_clip = 0.1f;
		camera.far_clip = 100.f;
		camera.azimuthal_angle = -geom::pi_f / 4.f;
		camera.elevation_angle = geom::pi_f / 6.f;
		camera.distance = 5.f;

		gl::LineWidth(5.f);
		gl::Enable(gl::DEPTH_TEST);

		glow::clear_color(glow::gray);

		middle_button_down = false;

		axes = renderer.create(glow::mesh::axes{});
	}

	void resize (int width, int height) override
	{
		camera.aspect_ratio = static_cast<float>(width) / height;

		renderer.transform(camera.transform());
	}

	void mouse_button_middle (bool down) override
	{
		middle_button_down = down;
	}

	void mouse_move (int x, int y, int dx, int dy) override
	{
		if (middle_button_down)
		{
			camera.azimuthal_angle += dx * 0.01f;
			camera.elevation_angle += dy * 0.01f;

			renderer.transform(camera.transform());
		}
	}

	void mouse_wheel (int delta) override
	{
		camera.distance *= std::pow(0.8f, delta);
		renderer.transform(camera.transform());
	}

	void draw ( ) override
	{
		gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

		renderer.render(axes);
	}
};

int main ( )
{
	run_demo<axes_demo>();
}
