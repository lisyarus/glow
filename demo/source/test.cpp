#include <glow/demo/common.hpp>

#include <glow/object/buffer.hpp>
#include <glow/object/vertex_array.hpp>
#include <glow/object/program.hpp>
#include <glow/object/texture.hpp>
#include <glow/color.hpp>

#include <geom/alias/point.hpp>

struct test_demo
	: basic_demo
{
	static constexpr geom::point_2f points[]
	{
		{ 0.f, 0.f },
		{ 1.f, 0.f },
		{ 1.f, 1.f },
		{ 0.f, 0.f },
		{ 1.f, 1.f },
		{ 0.f, 1.f },
	};

	static constexpr char vs[] =
R"(#version 330

layout (location = 0) in vec2 in_position;
layout (location = 1) in vec2 in_texcoord;

out vec2 texcoord;

void main ( )
{
	gl_Position = vec4(in_position, 0.0, 1.0);
	texcoord = in_texcoord;
}
)";

	static constexpr char fs[] =
R"(#version 330

uniform vec4 color;
uniform sampler2D tex;

in vec2 texcoord;

void main ( )
{
	gl_FragColor = texture2D(tex, texcoord);
}
)";

	glow::buffer triangle_vbo;
	glow::vertex_array triangle_vao;

	glow::program program;

	glow::texture tex;

	test_demo ( )
		: program {	glow::vertex_shader(vs), glow::fragment_shader(fs) }
	{
		triangle_vbo.bind_scoped(gl::ARRAY_BUFFER)
			.data(points)
			.with([&]{
				triangle_vao.bind_scoped()
					.attrib<geom::point_2f>(0, gl::FALSE_, 0, nullptr)
					.attrib<geom::point_2f>(1, gl::FALSE_, 0, nullptr);
			});

		glow::color_4ub pixels[] {
			glow::red, glow::green, glow::blue,
			glow::cyan, glow::magenta, glow::yellow,
		};

		tex.bind_scoped(gl::TEXTURE_2D)
			.parameter(gl::TEXTURE_MIN_FILTER, gl::NEAREST)
			.parameter(gl::TEXTURE_MAG_FILTER, gl::NEAREST)
			.image_2d(0, {3, 2}, pixels);

		glow::clear_color(glow::light(glow::light(glow::blue)));
	}

	void draw ( ) override
	{
		gl::Clear(gl::COLOR_BUFFER_BIT);

		program.bind_scoped()
			.uniform("tex", 0)
			.with([&]{
				tex.bind_scoped(gl::TEXTURE_2D)
					.with([&]{
						triangle_vao.bind_scoped()
							.draw(gl::TRIANGLES, 0, 6);
					});
			});
	}
};

int main ( )
{
	run_demo<test_demo>();
}
