#pragma once

#include <functional>
#include <memory>
#include <chrono>

struct basic_demo
{
	basic_demo ( )
		: start_(clock::now())
	{ }

	virtual void resize (int width, int height);

	virtual void draw ( );

	virtual void mouse_move (int x, int y, int dx, int dy) { }

	virtual void mouse_button_left   (bool down) { }
	virtual void mouse_button_middle (bool down) { }
	virtual void mouse_button_right  (bool down) { }

	virtual void mouse_wheel (int delta) { }

	virtual bool show_mouse ( ) const { return true; }

	virtual ~ basic_demo ( ) { }

protected:

	float time ( ) { return std::chrono::duration_cast<std::chrono::duration<float>>(clock::now() - start_).count(); }

private:
	using clock = std::chrono::steady_clock;

	clock::time_point start_;
};

void run_demo (std::function<std::unique_ptr<basic_demo>()> factory);

template <typename Demo>
void run_demo ( )
{
	run_demo([]{ return std::make_unique<Demo>(); });
}
