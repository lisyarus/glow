#include <glow/error.hpp>

namespace glow::detail
{

	const char * error_enum_to_string (GLenum error)
	{
		switch (error)
		{
		case gl::NO_ERROR_: return "no error";
		case gl::INVALID_ENUM: return "GL_INVALID_ENUM";
		case gl::INVALID_VALUE: return "GL_INVALID_VALUE";
		case gl::INVALID_OPERATION: return "GL_INVALID_OPERATION";
		case gl::OUT_OF_MEMORY: return "GL_OUT_OF_MEMORY";
		default: return "unknown error";
		}
	}

}
