#include <glow/renderer/colored.hpp>

static const char vs_source[] =
R"(#version 330

uniform mat4 u_transform;

layout (location = 0) in vec4 in_position;
layout (location = 1) in vec4 in_color;

out vec4 color;

void main ( )
{
	gl_Position = u_transform * in_position;
	color = in_color;
}

)";

static const char fs_source[] =
R"(#version 330

in vec4 color;

out vec4 out_color;

void main ( )
{
	out_color = color;
}

)";

glow::renderer::colored::colored ( )
	: renderer_base(glow::vertex_shader(vs_source), glow::fragment_shader(fs_source))
{ }

void glow::renderer::colored::transform (geom::matrix_4x4f const & m)
{
	program().bind_scoped().uniform("u_transform", m);
}
