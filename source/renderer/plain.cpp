#include <glow/renderer/plain.hpp>

static const char vs_source[] =
R"(#version 330

uniform mat4 u_transform;

layout (location = 0) in vec4 in_position;

void main ( )
{
	gl_Position = u_transform * in_position;
}

)";

static const char fs_source[] =
R"(#version 330

uniform vec4 u_color;

out vec4 out_color;

void main ( )
{
	out_color = u_color;
}

)";

glow::renderer::plain::plain ( )
	: renderer_base(glow::vertex_shader(vs_source), glow::fragment_shader(fs_source))
{ }

void glow::renderer::plain::transform (geom::matrix_4x4f const & m)
{
	program().bind_scoped().uniform("u_transform", m);
}

void glow::renderer::plain::color (glow::color_4f const & c)
{
	program().bind_scoped().uniform("u_color", c);
}
