#pragma once

#include <glow/gl.hpp>
#include <util/to_string.hpp>
#include <stdexcept>
#include <iostream>

namespace glow
{

	inline void load_functions ( )
	{
		if (!gl::sys::LoadFunctions())
		{
			throw std::runtime_error(util::to_string("failed to load OpenGL functions"));
		}
	}

	inline bool load_functions (std::nothrow_t)
	{
		return gl::sys::LoadFunctions();
	}

	 template <typename T>
	 T get (GLenum parameter);

	 template < >
	 inline GLboolean get<GLboolean> (GLenum parameter)
	 {
		 GLboolean value;
		 gl::GetBooleanv(parameter, &value);
		 return value;
	 }

	 template < >
	 inline GLint get<GLint> (GLenum parameter)
	 {
		 GLint value;
		 gl::GetIntegerv(parameter, &value);
		 return value;
	 }

	 template < >
	 inline GLfloat get<GLfloat> (GLenum parameter)
	 {
		 GLfloat value;
		 gl::GetFloatv(parameter, &value);
		 return value;
	 }

	 template < >
	 inline std::string get<std::string> (GLenum parameter)
	 {
		 return std::string(reinterpret_cast<const char *>(gl::GetString(parameter)));
	 }

	struct version
	{
		GLint major, minor;
	};

	inline std::ostream & operator << (std::ostream & os, version v)
	{
		return os << v.major << '.' << v.minor;
	}

	namespace context
	{
		inline glow::version version ( )
		{
			return { get<GLint>(gl::MAJOR_VERSION), get<GLint>(gl::MINOR_VERSION) };
		}

		inline std::string vendor ( )
		{
			return get<std::string>(gl::VENDOR);
		}

		inline std::string renderer ( )
		{
			return get<std::string>(gl::RENDERER);
		}
	}

}
