#pragma once

#include <geom/alias/vector.hpp>
#include <geom/alias/point.hpp>
#include <geom/alias/matrix.hpp>
#include <geom/operations/matrix.hpp>

namespace glow
{

	struct camera
	{
		virtual geom::matrix_4x4f projection ( ) const = 0;
		virtual geom::matrix_4x4f view ( ) const = 0;

		virtual geom::matrix_4x4f transform ( ) const
		{
			return projection() * view();
		}

		virtual geom::point_3f position ( ) const
		{
			// Highly inefficient implementation
			// A concrete camera better provide its own
			auto const p = geom::inverse(view()) * geom::vector_4f{0.f, 0.f, 0.f, 1.f};
			return { p[0], p[1], p[2] };
		}

		virtual geom::vector_3f direction ( ) const
		{
			// Highly inefficient implementation
			// A concrete camera better provide its own
			auto const d = geom::inverse(view()) * geom::vector_4f{0.f, 0.f, -1.f, 0.f};
			return { d[0], d[1], d[2] };
		}

		virtual std::array<geom::vector_4f, 6> clip_planes ( ) const
		{
			auto const m = geom::transpose(transform());

			std::array<geom::vector_4f, 6> p;

			p[0] = m * geom::vector_4f{1.f, 0.f, 0.f, 1.f};
			p[1] = m * geom::vector_4f{-1.f, 0.f, 0.f, 1.f};
			p[2] = m * geom::vector_4f{0.f, 1.f, 0.f, 1.f};
			p[3] = m * geom::vector_4f{0.f, -1.f, 0.f, 1.f};
			p[4] = m * geom::vector_4f{0.f, 0.f, 1.f, 1.f};
			p[5] = m * geom::vector_4f{0.f, 0.f, -1.f, 1.f};

			return p;
		}

		virtual ~ camera ( ) { }
	};

}
