#pragma once

#include <geom/transform/perspective.hpp>

#include <glow/camera/base.hpp>

namespace glow
{

	struct perspective_camera
		: camera
	{
		float fov_x;
		float aspect_ratio; // fov_x / fov_y

		float near_clip;
		float far_clip;

		geom::matrix_4x4f projection ( ) const override
		{
			return geom::perspective<float, 3>(fov_x, aspect_ratio, near_clip, far_clip).homogeneous_matrix();
		}
	};

}
