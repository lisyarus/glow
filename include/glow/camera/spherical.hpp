#pragma once

#include <geom/transform/translation.hpp>
#include <geom/transform/rotation.hpp>
#include <geom/transform/homogeneous.hpp>
#include <geom/transform/permutation.hpp>

#include <glow/camera/perspective.hpp>

namespace glow
{

	struct spherical_camera
		: perspective_camera
	{
		float azimuthal_angle;
		float elevation_angle;
		float distance;
		geom::point_3f target;

		geom::matrix_4x4f view ( ) const override
		{
			return geom::matrix_4x4f::identity()
				* geom::translation<float, 3>({0.f, 0.f, -distance}).homogeneous_matrix()
				* geom::homogeneous(geom::plane_rotation<float, 3>(1, 2, elevation_angle).matrix())
				* geom::homogeneous(geom::plane_rotation<float, 3>(2, 0, azimuthal_angle).matrix())
				* geom::translation<float, 3>({ -target[0], -target[1], -target[2] }).homogeneous_matrix()
				;
		}

		geom::vector_3f direction ( ) const override
		{
			return {
				- std::cos(elevation_angle) * std::sin(azimuthal_angle),
				std::sin(elevation_angle),
				std::cos(elevation_angle) * std::cos(azimuthal_angle),
			};
		}
	};

}
