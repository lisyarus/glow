#pragma once

#include <glow/gl.hpp>

#include <geom/alias/vector.hpp>

namespace glow
{

	using color_1ub = geom::vector_1ub;
	using color_2ub = geom::vector_2ub;
	using color_3ub = geom::vector_3ub;
	using color_4ub = geom::vector_4ub;

	using color_1f = geom::vector_1f;
	using color_2f = geom::vector_2f;
	using color_3f = geom::vector_3f;
	using color_4f = geom::vector_4f;

	template <typename T>
	struct color_traits;

	template < >
	struct color_traits<color_1ub>
	{
		static constexpr GLenum format = gl::RED;
		static constexpr GLenum type = gl::UNSIGNED_BYTE;
	};

	template < >
	struct color_traits<color_2ub>
	{
		static constexpr GLenum format = gl::RG;
		static constexpr GLenum type = gl::UNSIGNED_BYTE;
	};

	template < >
	struct color_traits<color_3ub>
	{
		static constexpr GLenum format = gl::RGB;
		static constexpr GLenum type = gl::UNSIGNED_BYTE;
	};

	template < >
	struct color_traits<color_4ub>
	{
		static constexpr GLenum format = gl::RGBA;
		static constexpr GLenum type = gl::UNSIGNED_BYTE;
	};

	template < >
	struct color_traits<color_1f>
	{
		static constexpr GLenum format = gl::RED;
		static constexpr GLenum type = gl::FLOAT;
	};

	template < >
	struct color_traits<color_2f>
	{
		static constexpr GLenum format = gl::RG;
		static constexpr GLenum type = gl::FLOAT;
	};

	template < >
	struct color_traits<color_3f>
	{
		static constexpr GLenum format = gl::RGB;
		static constexpr GLenum type = gl::FLOAT;
	};

	template < >
	struct color_traits<color_4f>
	{
		static constexpr GLenum format = gl::RGBA;
		static constexpr GLenum type = gl::FLOAT;
	};

	namespace detail
	{

		struct generic_color
		{
		private:

			static constexpr std::uint8_t to_ub (float x)
			{
				return static_cast<uint8_t>(x * 255.f);
			}

		public:

			float r, g, b;

			constexpr operator color_3f() const
			{
				return { r, g, b };
			}

			constexpr operator color_4f() const
			{
				return { r, g, b, 1.f };
			}

			constexpr operator color_3ub() const
			{
				return { to_ub(r), to_ub(g), to_ub(b) };
			}

			constexpr operator color_4ub() const
			{
				return { to_ub(r), to_ub(g), to_ub(b), 255 };
			}

		};

		template <typename T>
		T color_max ( );

		template < >
		inline std::uint8_t color_max<std::uint8_t> ( )
		{
			return 255;
		}

		template < >
		inline float color_max<float> ( )
		{
			return 1.f;
		}

		template <typename Color>
		Color color_white ( )
		{
			Color c;
			for (std::size_t i = 0; i < Color::dimension; ++i)
				c[i] = color_max<typename Color::value_type>();
			return c;
		}

		inline color_4f to_color_4f (color_3ub const & c)
		{
			color_3f cf = geom::cast<float>(c) / 255.f;
			return { cf[0], cf[1], cf[2], 1.f };
		}

		inline color_4f to_color_4f (color_4ub const & c)
		{
			return geom::cast<float>(c) / 255.f;
		}

		inline color_4f to_color_4f (color_3f const & c)
		{
			return { c[0], c[1], c[2], 1.f };
		}

		inline color_4f to_color_4f (color_4f const & c)
		{
			return c;
		}

		inline color_4f to_color_4f (generic_color const & c)
		{
			return static_cast<color_4f>(c);
		}

	}

	inline constexpr detail::generic_color black    { 0.f, 0.f, 0.f };
	inline constexpr detail::generic_color white    { 1.f, 1.f, 1.f };

	inline constexpr detail::generic_color red      { 1.f, 0.f, 0.f };
	inline constexpr detail::generic_color green    { 0.f, 1.f, 0.f };
	inline constexpr detail::generic_color blue     { 0.f, 0.f, 1.f };
	inline constexpr detail::generic_color cyan     { 0.f, 1.f, 1.f };
	inline constexpr detail::generic_color magenta  { 1.f, 0.f, 1.f };
	inline constexpr detail::generic_color yellow   { 1.f, 1.f, 0.f };
	inline constexpr detail::generic_color gray     { 0.5f, 0.5f, 0.5f };

	template <typename Color>
	inline constexpr Color dark (Color const & c)
	{
		return c / static_cast<typename Color::value_type>(2);
	}

	inline constexpr detail::generic_color dark (detail::generic_color const & c)
	{
		return { c.r / 2.f, c.g / 2.f, c.b / 2.f };
	}

	template <typename Color>
	inline constexpr Color light (Color const & c)
	{
		return c + (detail::color_white<Color>() - c) / static_cast<typename Color::value_type>(2);
	}

	inline constexpr detail::generic_color light (detail::generic_color const & c)
	{
		return { c.r + (1.f - c.r) / 2.f, c.g + (1.f - c.g) / 2.f, c.b + (1.f - c.b) / 2.f };
	}

	template <typename Color>
	void clear_color (Color const & c)
	{
		auto cf = detail::to_color_4f(c);
		gl::ClearColor(cf[0], cf[1], cf[2], cf[3]);
	}

}
