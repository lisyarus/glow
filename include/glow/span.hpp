#pragma once

#include <cstddef>
#include <type_traits>
#include <array>
#include <vector>

namespace glow
{

	template <typename T>
	struct span
	{
		using pointer = T const *;
		using iterator = pointer;

		span ( )
			: begin_(nullptr)
			, end_(nullptr)
		{ }

		span (pointer begin, pointer end)
			: begin_(begin)
			, end_(end)
		{ }

		template <std::size_t N>
		span (T const (&a)[N])
			: begin_(a)
			, end_(a + N)
		{ }

		template <std::size_t N>
		span (std::array<T, N> const & a)
			: begin_(a.data())
			, end_(a.data() + a.size())
		{ }

		span (std::vector<T> const & v)
			: begin_(v.data())
			, end_(v.data() + v.size())
		{ }

		span (span const &) = default;

		pointer begin ( ) const
		{
			return begin_;
		}

		pointer end ( ) const
		{
			return end_;
		}

		std::size_t size ( ) const
		{
			return end() - begin();
		}

		pointer data ( ) const
		{
			return begin_;
		}

	private:
		pointer begin_;
		pointer end_;
	};

}
