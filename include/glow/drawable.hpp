#pragma once

#include <glow/object/buffer.hpp>
#include <glow/object/vertex_array.hpp>

namespace glow
{

	struct drawable
	{
		glow::buffer vbo;
		glow::vertex_array vao;

		GLuint count;
		GLenum mode;

		drawable ( )
			: vbo(std::nullopt)
			, vao(std::nullopt)
			, count(0)
			, mode(0)
		{ }

		drawable (drawable && other)
			: vbo(std::move(other.vbo))
			, vao(std::move(other.vao))
			, count(other.count)
			, mode(other.mode)
		{
			other.count = 0;
			other.mode = 0;
		}

		drawable & operator = (drawable && other)
		{
			vbo = std::move(other.vbo);
			vao = std::move(other.vao);
			count = other.count;
			mode = other.mode;
			other.count = 0;
			other.mode = 0;
			return *this;
		}

		void draw ( )
		{
			if (count)
				vao.bind_scoped().draw(mode, 0, count);
		}
	};

}
