#pragma once

#include <glow/object/base.hpp>
#include <glow/color.hpp>

#include <geom/alias/vector.hpp>

namespace glow
{

	namespace detail
	{

		inline GLuint create_texture_id ( )
		{
			GLuint id;
			gl::GenTextures(1, &id);
			return id;
		}

		inline void delete_texture_id (GLuint id)
		{
			gl::DeleteTextures(1, &id);
		}

	}

	template <bool Scoped>
	struct texture_binder
		: detail::binder<texture_binder<Scoped>, Scoped>
	{
		GLenum target;

		texture_binder (GLenum target)
			: target(target)
		{ }

		void unbind ( )
		{
			gl::BindTexture(target, 0);
		}

		texture_binder & generate_mipmap ( )
		{
			gl::GenerateMipmap(target);
			return *this;
		}

		texture_binder & parameter (GLenum param, GLint value)
		{
			gl::TexParameteri(target, param, value);
			return *this;
		}

		texture_binder & parameter (GLenum param, GLfloat value)
		{
			gl::TexParameterf(target, param, value);
			return *this;
		}

		texture_binder & parameter (GLenum param, GLint const * value)
		{
			gl::TexParameteriv(target, param, value);
			return *this;
		}

		texture_binder & parameter (GLenum param, GLfloat const * value)
		{
			gl::TexParameterfv(target, param, value);
			return *this;
		}

		texture_binder & parameter (GLenum param, GLuint const * value)
		{
			gl::TexParameterIuiv(target, param, value);
			return *this;
		}

		texture_binder & parameter (GLenum param, decltype(gl::NEAREST) value)
		{
			gl::TexParameteri(target, param, value);
			return *this;
		}

		// Direct image load

		texture_binder & image_1d (GLint level, GLint internalformat, GLsizei size, GLenum format, GLenum type, void const * pixels)
		{
			gl::TexImage1D(target, level, internalformat, size, 0, format, type, pixels);
			return *this;
		}

		texture_binder & image_2d (GLint level, GLint internalformat, geom::vector_2i size, GLenum format, GLenum type, void const * pixels)
		{
			gl::TexImage2D(target, level, internalformat, size[0], size[1], 0, format, type, pixels);
			return *this;
		}

		texture_binder & image_3d (GLint level, GLint internalformat, geom::vector_3i size, GLenum format, GLenum type, void const * pixels)
		{
			gl::TexImage3D(target, level, internalformat, size[0], size[1], size[2], 0, format, type, pixels);
			return *this;
		}

		// Pixel format & type get deduces from Color type

		template <typename Color>
		texture_binder & image_1d (GLint level, GLint internalformat, GLsizei size, Color const * pixels)
		{
			using traits = color_traits<Color>;
			return image_1d(level, internalformat, size, traits::format, traits::type, pixels);
		}

		template <typename Color>
		texture_binder & image_2d (GLint level, GLint internalformat, geom::vector_2i size, Color const * pixels)
		{
			using traits = color_traits<Color>;
			return image_2d(level, internalformat, size, traits::format, traits::type, pixels);
		}

		template <typename Color>
		texture_binder & image_3d (GLint level, GLint internalformat, geom::vector_3i size, Color const * pixels)
		{
			using traits = color_traits<Color>;
			return image_3d(level, internalformat, size, traits::format, traits::type, pixels);
		}

		// Internal format assumed to be equal to pixel format

		template <typename Color>
		texture_binder & image_1d (GLint level, GLsizei size, Color const * pixels)
		{
			using traits = color_traits<Color>;
			return image_1d(level, traits::format, size, traits::format, traits::type, pixels);
		}

		template <typename Color>
		texture_binder & image_2d (GLint level, geom::vector_2i size, Color const * pixels)
		{
			using traits = color_traits<Color>;
			return image_2d(level, traits::format, size, traits::format, traits::type, pixels);
		}

		template <typename Color>
		texture_binder & image_3d (GLint level, geom::vector_3i size, Color const * pixels)
		{
			using traits = color_traits<Color>;
			return image_3d(level, traits::format, size, traits::format, traits::type, pixels);
		}
	};

	struct texture
		: detail::object<texture>
	{

		texture ( )
			: object(detail::create_texture_id())
		{ }

		texture (std::nullopt_t)
		{ }

		texture_binder<false> bind (GLenum target)
		{
			gl::BindTexture(target, id());
			return {target};
		}

		texture_binder<true> bind_scoped (GLenum target)
		{
			gl::BindTexture(target, id());
			return {target};
		}

	private:

		friend struct detail::object_access;

		void do_reset ( )
		{
			detail::delete_texture_id(id());
		}
	};

}
