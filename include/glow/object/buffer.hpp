#pragma once

#include <glow/object/base.hpp>
#include <glow/span.hpp>

#include <optional>

namespace glow
{

	namespace detail
	{

		inline GLuint create_buffer_id ( )
		{
			GLuint id;
			gl::GenBuffers(1, &id);
			return id;
		}

		inline void delete_buffer_id (GLuint id)
		{
			gl::DeleteBuffers(1, &id);
		}

	}

	template <bool Scoped>
	struct buffer_binder
		: detail::binder<buffer_binder<Scoped>, Scoped>
	{
		GLenum target;

		buffer_binder (GLenum target)
			: target(target)
		{ }

		void unbind ( )
		{
			gl::BindBuffer(target, 0);
		}

		template <typename T>
		buffer_binder & data (T const * ptr, GLsizeiptr size, GLenum usage = gl::STATIC_DRAW)
		{
			gl::BufferData(target, size * sizeof(T), ptr, usage);
			return *this;
		}

		buffer_binder & data (std::nullptr_t, GLsizeiptr size, GLenum usage = gl::STATIC_DRAW)
		{
			gl::BufferData(target, size, nullptr, usage);
			return *this;
		}

		template <typename T>
		buffer_binder & data (span<T> s, GLenum usage = gl::STATIC_DRAW)
		{
			return data(s.data(), s.size(), usage);
		}

		template <typename Array>
		buffer_binder & data (Array const & a, GLenum usage = gl::STATIC_DRAW)
		{
			return data(span(a), usage);
		}
	};

	struct buffer
		: detail::object<buffer>
	{
		buffer ( )
			: object(detail::create_buffer_id())
		{ }

		buffer (std::nullopt_t)
		{ }

		buffer_binder<false> bind (GLenum target)
		{
			gl::BindBuffer(target, id());
			return { target };
		}

		buffer_binder<true> bind_scoped (GLenum target)
		{
			gl::BindBuffer(target, id());
			return { target };
		}

	private:
		friend detail::object_access;

		void do_reset ( )
		{
			detail::delete_buffer_id(id());
		}
	};

}
