#pragma once

#include <glow/object/base.hpp>
#include <glow/object/texture.hpp>
#include <glow/object/renderbuffer.hpp>

namespace glow
{

	namespace detail
	{

		const char * framebuffer_status_to_string (GLenum status);

	}

	struct framebuffer_not_complete
		: std::runtime_error
	{
		framebuffer_not_complete (GLenum status)
			: std::runtime_error::runtime_error(std::string("framebuffer not complete: ")
				+ detail::framebuffer_status_to_string(status))
		{ }
	};

	namespace detail
	{

		inline GLuint create_framebuffer_id ( )
		{
			GLuint id;
			gl::GenFramebuffers(1, &id);
			return id;
		}

		inline void delete_framebuffer_id (GLuint id)
		{
			gl::DeleteFramebuffers(1, &id);
		}

	}

	template <bool Scoped>
	struct framebuffer_binder
		: detail::binder<framebuffer_binder<Scoped>, Scoped>
	{
		GLenum target;

		framebuffer_binder (GLenum target)
			: target(target)
		{ }

		void unbind ( )
		{
			gl::BindFramebuffer(target, 0);
		}

		framebuffer_binder & attach_color (texture const & tex, int attachment = 0, int level = 0)
		{
			gl::FramebufferTexture(target, gl::COLOR_ATTACHMENT0 + attachment, tex.id(), level);
			return *this;
		}

		framebuffer_binder & attach_color (renderbuffer const & rb, int attachment = 0)
		{
			gl::FramebufferRenderbuffer(target, gl::COLOR_ATTACHMENT0 + attachment, gl::RENDERBUFFER, rb.id());
			return *this;
		}

		framebuffer_binder & attach_depth (texture const & tex, int level = 0)
		{
			gl::FramebufferTexture(target, gl::DEPTH_ATTACHMENT, tex.id(), level);
			return *this;
		}

		framebuffer_binder & attach_depth (renderbuffer const & rb)
		{
			gl::FramebufferRenderbuffer(target, gl::DEPTH_ATTACHMENT, gl::RENDERBUFFER, rb.id());
			return *this;
		}

		template <typename ... Buffers>
		framebuffer_binder & draw_buffers (Buffers ... buffers)
		{
			GLenum b[] { static_cast<GLenum>(buffers)... };
			gl::DrawBuffers(sizeof...(Buffers), b);
			return *this;
		}

		GLenum status ( ) const
		{
			return gl::CheckFramebufferStatus(target);
		}

		bool complete ( ) const
		{
			return status() == gl::FRAMEBUFFER_COMPLETE;
		}

		framebuffer_binder & assert_complete ( )
		{
			auto s = status();
			if (s != gl::FRAMEBUFFER_COMPLETE)
				throw framebuffer_not_complete{s};
			return *this;
		}
	};

	struct framebuffer
		: detail::object<framebuffer>
	{
		framebuffer ( )
			: object(detail::create_framebuffer_id())
		{ }

		framebuffer (std::nullopt_t)
		{ }

		framebuffer_binder<false> bind (GLenum target = gl::FRAMEBUFFER)
		{
			gl::BindFramebuffer(target, id());
			return {target};
		}

		framebuffer_binder<true> bind_scoped (GLenum target = gl::FRAMEBUFFER)
		{
			gl::BindFramebuffer(target, id());
			return {target};
		}

	private:

		friend struct detail::object_access;

		void do_reset ( )
		{
			detail::delete_framebuffer_id(id());
		}
	};

}
