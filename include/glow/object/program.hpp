#pragma once

#include <glow/object/base.hpp>
#include <glow/object/shader.hpp>
#include <glow/color.hpp>

#include <geom/alias/vector.hpp>
#include <geom/alias/point.hpp>
#include <geom/alias/matrix.hpp>

#include <stdexcept>
#include <unordered_map>

namespace glow
{

	struct program_link_error
		: std::runtime_error
	{
		using std::runtime_error::runtime_error;
	};

	struct no_such_uniform_error
		: std::runtime_error
	{
		no_such_uniform_error (std::string const & uniform_name)
			: std::runtime_error::runtime_error("\"" + uniform_name + "\": no such uniform")
		{ }
	};

	namespace detail
	{

		inline void attach (GLuint program_id)
		{ }

		template <typename ... Shaders>
		void attach (GLuint program_id, shader const & first, Shaders const & ... shaders)
		{
			gl::AttachShader(program_id, first.id());
			attach(program_id, shaders...);
		}

		inline void detach (GLuint program_id)
		{ }

		template <typename ... Shaders>
		void detach (GLuint program_id, shader const & first, Shaders const & ... shaders)
		{
			gl::DetachShader(program_id, first.id());
			detach(program_id, shaders...);
		}

		inline void set_uniform (GLint location, int v0)
		{
			gl::Uniform1i(location, v0);
		}

		inline void set_uniform (GLint location, int v0, int v1)
		{
			gl::Uniform2i(location, v0, v1);
		}

		inline void set_uniform (GLint location, int v0, int v1, int v2)
		{
			gl::Uniform3i(location, v0, v1, v2);
		}

		inline void set_uniform (GLint location, int v0, int v1, int v2, int v3)
		{
			gl::Uniform4i(location, v0, v1, v2, v3);
		}

		inline void set_uniform (GLint location, geom::vector_1i const & v)
		{
			gl::Uniform1i(location, v[0]);
		}

		inline void set_uniform (GLint location, geom::vector_2i const & v)
		{
			gl::Uniform2i(location, v[0], v[1]);
		}

		inline void set_uniform (GLint location, geom::vector_3i const & v)
		{
			gl::Uniform3i(location, v[0], v[1], v[2]);
		}

		inline void set_uniform (GLint location, geom::vector_4i const & v)
		{
			gl::Uniform4i(location, v[0], v[1], v[2], v[3]);
		}

		inline void set_uniform (GLint location, geom::point_1i const & v)
		{
			gl::Uniform1i(location, v[0]);
		}

		inline void set_uniform (GLint location, geom::point_2i const & v)
		{
			gl::Uniform2i(location, v[0], v[1]);
		}

		inline void set_uniform (GLint location, geom::point_3i const & v)
		{
			gl::Uniform3i(location, v[0], v[1], v[2]);
		}

		inline void set_uniform (GLint location, geom::point_4i const & v)
		{
			gl::Uniform4i(location, v[0], v[1], v[2], v[3]);
		}

		inline void set_uniform (GLint location, float v0)
		{
			gl::Uniform1f(location, v0);
		}

		inline void set_uniform (GLint location, float v0, float v1)
		{
			gl::Uniform2f(location, v0, v1);
		}

		inline void set_uniform (GLint location, float v0, float v1, float v2)
		{
			gl::Uniform3f(location, v0, v1, v2);
		}

		inline void set_uniform (GLint location, float v0, float v1, float v2, float v3)
		{
			gl::Uniform4f(location, v0, v1, v2, v3);
		}

		inline void set_uniform (GLint location, geom::vector_1f const & v)
		{
			gl::Uniform1f(location, v[0]);
		}

		inline void set_uniform (GLint location, geom::vector_2f const & v)
		{
			gl::Uniform2f(location, v[0], v[1]);
		}

		inline void set_uniform (GLint location, geom::vector_3f const & v)
		{
			gl::Uniform3f(location, v[0], v[1], v[2]);
		}

		inline void set_uniform (GLint location, geom::vector_4f const & v)
		{
			gl::Uniform4f(location, v[0], v[1], v[2], v[3]);
		}

		inline void set_uniform (GLint location, geom::point_1f const & v)
		{
			gl::Uniform1f(location, v[0]);
		}

		inline void set_uniform (GLint location, geom::point_2f const & v)
		{
			gl::Uniform2f(location, v[0], v[1]);
		}

		inline void set_uniform (GLint location, geom::point_3f const & v)
		{
			gl::Uniform3f(location, v[0], v[1], v[2]);
		}

		inline void set_uniform (GLint location, geom::point_4f const & v)
		{
			gl::Uniform4f(location, v[0], v[1], v[2], v[3]);
		}

		inline void set_uniform (GLint location, geom::matrix_2x2f const & m)
		{
			gl::UniformMatrix2fv(location, 1, gl::TRUE_, m.data()->data());
		}

		inline void set_uniform (GLint location, geom::matrix_3x3f const & m)
		{
			gl::UniformMatrix3fv(location, 1, gl::TRUE_, m.data()->data());
		}

		inline void set_uniform (GLint location, geom::matrix_4x4f const & m)
		{
			gl::UniformMatrix4fv(location, 1, gl::TRUE_, m.data()->data());
		}

		inline void set_uniform (GLint location, generic_color const & c)
		{
			gl::Uniform4f(location, c.r, c.g, c.b, 1.f);
		}

	}

	struct program;

	template <bool Scoped>
	struct program_binder
		: detail::binder<program_binder<Scoped>, Scoped>
	{
		program * p;

		program_binder ( )
			: p{ nullptr }
		{ }

		program_binder (program * p)
			: p{ p }
		{ }

		program_binder (program_binder && other)
			: p{ other.p }
		{
			other.p = nullptr;
		}

		program_binder & operator = (program_binder && other)
		{
			if (std::addressof(other) != this)
			{
				unbind();
				std::swap(p, other.p);
			}

			return *this;
		}

		void unbind ( )
		{
			if (p)
			{
				gl::UseProgram(0);
				p = nullptr;
			}
		}

		template <typename ... T>
		program_binder & uniform (std::string const & name, T const & ... value);

		template <typename ... T>
		program_binder & uniform (std::string const & name, std::nothrow_t, T const & ... value);
	};

	struct program
		: detail::object<program>
	{
		program ( )
			: object(gl::CreateProgram())
		{ }

		program (std::nullopt_t)
		{ }

		template <typename ... Shaders>
		program (Shaders const & ... shaders)
			: program()
		{
			link(shaders...);
		}

		template <typename ... Shaders>
		void link (Shaders const & ... shaders)
		{
			link(std::nothrow, shaders...);
			if (!linked())
				throw program_link_error(info_log());
		}

		template <typename ... Shaders>
		void link (std::nothrow_t, Shaders const & ... shaders)
		{
			detail::attach(id(), shaders...);
			gl::LinkProgram(id());
			detail::detach(id(), shaders...);
		}

		bool linked ( ) const
		{
			GLint link_status;
			gl::GetProgramiv(id(), gl::LINK_STATUS, &link_status);
			return link_status == gl::TRUE_;
		}

		std::string info_log ( ) const
		{
			std::unique_ptr<char[]> log;
			GLint length;
			gl::GetProgramiv(id(), gl::INFO_LOG_LENGTH, &length);
			log.reset(new char [length]);
			gl::GetProgramInfoLog(id(), length, nullptr, log.get());
			return detail::to_string(log, length);
		}

		GLint uniform_location (std::string const & name) const
		{
			return uniform_location_impl<false>(name);
		}

		GLint uniform_location (std::string const & name, std::nothrow_t) const
		{
			return uniform_location_impl<true>(name);
		}

		program_binder<false> bind ( )
		{
			gl::UseProgram(id());
			return {this};
		}

		program_binder<true> bind_scoped ( )
		{
			gl::UseProgram(id());
			return {this};
		}

	private:

		friend struct detail::object_access;

		void do_reset ( )
		{
			gl::DeleteProgram(id());
		}

		mutable std::unordered_map<std::string, GLint> uniform_location_cache;

		template <bool NoThrow>
		GLint uniform_location_impl (std::string const & name) const
		{
			if (auto it = uniform_location_cache.find(name); it != uniform_location_cache.end())
			{
				return it->second;
			}
			else
			{
				GLint location = gl::GetUniformLocation(id(), name.data());
				if constexpr (!NoThrow)
				{
					if (location == -1)
						throw no_such_uniform_error(name);
				}
				uniform_location_cache[name] = location;
				return location;
			}
		}
	};

	template <bool Scoped>
	template <typename ... T>
	program_binder<Scoped> & program_binder<Scoped>::uniform (std::string const & name, T const & ... value)
	{
		detail::set_uniform(p->uniform_location(name), value...);
		return *this;
	}

	template <bool Scoped>
	template <typename ... T>
	program_binder<Scoped> & program_binder<Scoped>::uniform (std::string const & name, std::nothrow_t, T const & ... value)
	{
		detail::set_uniform(p->uniform_location(name, std::nothrow), value...);
		return *this;
	}

}
