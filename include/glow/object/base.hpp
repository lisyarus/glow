#pragma once

#include <glow/gl.hpp>
#include <optional>
#include <type_traits>

namespace glow
{
namespace detail
{

	struct object_access
	{
	protected:
		template <typename Object>
		static void call_reset (Object & o)
		{
			o.do_reset();
		}
	};

	template <typename Derived>
	struct object
		: object_access
	{
	private:
		GLuint id_;

		Derived & derived()
		{
			return static_cast<Derived &>(*this);
		}

		Derived const & derived() const
		{
			return static_cast<Derived &>(*this);
		}

	protected:

		object ( )
			: id_(0)
		{ }

		object (GLuint id)
			: id_(id)
		{ }

		object (object && other)
			: id_(other.id_)
		{
			other.id_ = 0;
		}

		object & operator = (object && other)
		{
			reset();
			std::swap(id_, other.id_);
			return *this;
		}

	public:

		GLuint id ( ) const
		{
			return id_;
		}

		~ object ( )
		{
			reset();
		}

		explicit operator bool ( ) const
		{
			return id_ != 0;
		}

		bool operator ! ( ) const
		{
			return id_ == 0;
		}

		void reset ( )
		{
			call_reset(derived());
			id_ = 0;
		}

		static Derived & null ( )
		{
			static Derived obj { std::nullopt };
			return obj;
		}

	};

	template <typename Derived, bool Scoped>
	struct binder
	{
	private:

		Derived & derived()
		{
			return static_cast<Derived &>(*this);
		}

		Derived const & derived() const
		{
			return static_cast<Derived &>(*this);
		}

	public:

		binder ( ) = default;
		binder (binder &&) = default;
		binder & operator = (binder &&) = default;

		binder (binder const &) = delete;

		binder & operator = (binder const &) = delete;

		template <typename F>
		Derived & with (F && f)
		{
			if constexpr (std::is_invocable_v<F>)
			{
				std::forward<F>(f)();
			}
			else
			{
				std::forward<F>(f)(derived());
			}
			return derived();
		}

		~ binder ( )
		{
			if constexpr (Scoped)
			{
				derived().unbind();
			}
		}
	};

}
}
