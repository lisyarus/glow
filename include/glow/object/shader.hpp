#pragma once

#include <glow/object/base.hpp>

#include <stdexcept>
#include <string>
#include <string_view>
#include <memory>
#include <cstring>

namespace glow
{

	struct shader_compillation_error
		: std::runtime_error
	{
		using std::runtime_error::runtime_error;
	};

	namespace detail
	{

		inline const char * str_data (const char * str)
		{
			return str;
		}

		inline const char * str_data (std::string const & str)
		{
			return str.c_str();
		}

		inline const char * str_data (std::string_view str)
		{
			return str.data();
		}

		inline GLsizei str_length (const char * str)
		{
			auto l = std::strlen(str);
			if (str[l-1] == 0)
				--l;
			return l;
		}

		inline GLsizei str_length (std::string const & str)
		{
			return str.size();
		}

		inline GLsizei str_length (std::string_view str)
		{
			auto l = str.size();
			if (str.back() == 0)
				--l;
			return l;
		}

		inline std::string to_string (std::unique_ptr<char[]> const & str, GLsizei length)
		{
			std::string result;
			result.resize(length);
			std::copy(str.get(), str.get() + length, result.begin());
			return result;
		}

	}

	struct shader
		: detail::object<shader>
	{
		shader (GLenum type)
			: object(gl::CreateShader(type))
		{ }

		template <typename ... Sources>
		shader (GLenum type, Sources const & ... sources)
			: shader(type)
		{
			compile(sources...);
		}

		shader (std::nullopt_t)
		{ }

		template <typename ... Sources>
		void compile (Sources const & ... sources)
		{
			compile(std::nothrow, sources...);
			if (!compiled())
				throw shader_compillation_error(info_log());
		}

		template <typename ... Sources>
		void compile (std::nothrow_t, Sources const & ... sources)
		{
			const char * sources_data[] = {detail::str_data(sources)...};
			GLint sources_length[] = {detail::str_length(sources)...};

			gl::ShaderSource(id(), sizeof...(sources), sources_data, sources_length);
			gl::CompileShader(id());
		}

		bool compiled ( ) const
		{
			GLint compile_status;
			gl::GetShaderiv(id(), gl::COMPILE_STATUS, &compile_status);
			return compile_status == gl::TRUE_;
		}

		std::string info_log ( ) const
		{
			std::unique_ptr<char[]> log;
			GLsizei length;
			gl::GetShaderiv(id(), gl::INFO_LOG_LENGTH, &length);
			log.reset(new char [length]);
			gl::GetShaderInfoLog(id(), length, nullptr, log.get());
			return detail::to_string(log, length);
		}

	private:

		friend struct detail::object_access;

		void do_reset ( )
		{
			gl::DeleteShader(id());
		}
	};

	template <typename ... Sources>
	shader vertex_shader (Sources const & ... sources)
	{
		return shader { gl::VERTEX_SHADER, sources... };
	}

	template <typename ... Sources>
	shader geometry_shader (Sources const & ... sources)
	{
		return shader { gl::GEOMETRY_SHADER, sources... };
	}

	template <typename ... Sources>
	shader fragment_shader (Sources const & ... sources)
	{
		return shader { gl::FRAGMENT_SHADER, sources... };
	}

}
