#pragma once

#include <glow/object/base.hpp>

#include <geom/alias/vector.hpp>

namespace glow
{

	namespace detail
	{

		inline GLuint create_renderbuffer_id ( )
		{
			GLuint id;
			gl::GenRenderbuffers(1, &id);
			return id;
		}

		inline void delete_renderbuffer_id (GLuint id)
		{
			gl::DeleteRenderbuffers(1, &id);
		}

	}

	template <bool Scoped>
	struct renderbuffer_binder
		: detail::binder<renderbuffer_binder<Scoped>, Scoped>
	{
		GLenum target;

		renderbuffer_binder (GLenum target)
			: target(target)
		{ }

		void unbind ( )
		{
			gl::BindRenderbuffer(target, 0);
		}

		renderbuffer_binder & storage (GLenum internal_format, geom::vector_2i size)
		{
			gl::RenderbufferStorage(target, internal_format, size[0], size[1]);
			return *this;
		}
	};

	struct renderbuffer
		: detail::object<renderbuffer>
	{
		renderbuffer ( )
			: object(detail::create_renderbuffer_id())
		{ }

		renderbuffer (std::nullopt_t)
		{ }

		renderbuffer_binder<false> bind (GLenum target = gl::RENDERBUFFER)
		{
			gl::BindRenderbuffer(target, id());
			return {target};
		}

		renderbuffer_binder<true> bind_scoped (GLenum target = gl::RENDERBUFFER)
		{
			gl::BindRenderbuffer(target, id());
			return {target};
		}

	private:

		friend struct detail::object_access;

		void do_reset ( )
		{
			detail::delete_renderbuffer_id(id());
		}
	};

}
