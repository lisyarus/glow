#pragma once

#include <glow/attribute.hpp>
#include <glow/object/base.hpp>
#include <glow/object/buffer.hpp>

namespace glow
{

	namespace detail
	{

		inline GLuint create_vertex_array_id ( )
		{
			GLuint id;
			gl::GenVertexArrays(1, &id);
			return id;
		}

		inline void delete_vertex_array_id (GLuint id)
		{
			gl::DeleteVertexArrays(1, &id);
		}

	}

	template <bool Scoped>
	struct vertex_array_binder
		: detail::binder<vertex_array_binder<Scoped>, Scoped>
	{
		void unbind ( )
		{
			gl::BindVertexArray(0);
		}

		vertex_array_binder & attrib (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, void const * pointer)
		{
			gl::EnableVertexAttribArray(index);
			gl::VertexAttribPointer(index, size, type, normalized, stride, pointer);
			return *this;
		}

		template <typename Vector>
		vertex_array_binder & attrib (GLuint index, GLboolean normalized, GLsizei stride, void const * pointer)
		{
			GLint size = Vector::dimension;
			GLenum type = detail::gl_type_v<typename Vector::value_type>;
			return attrib(index, size, type, normalized, stride, pointer);
		}

		vertex_array_binder & attrib (GLuint index, attribute_description const & desc, void const * pointer = nullptr)
		{
			return attrib(index, desc.size, desc.type, desc.normalized, desc.stride, reinterpret_cast<char const *>(pointer) + desc.offset);
		}

		template <typename ... Args>
		vertex_array_binder & attrib (buffer & buf, Args && ... args)
		{
			buf.bind_scoped(gl::ARRAY_BUFFER).with([&]{ attrib(std::forward<Args>(args)...); });
			return *this;
		}

		vertex_array_binder & attrib_i (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, void const * pointer)
		{
			gl::EnableVertexAttribArray(index);
			gl::VertexAttribIPointer(index, size, type, stride, pointer);
			return *this;
		}

		template <typename Vector>
		vertex_array_binder & attrib_i (GLuint index, GLboolean normalized, GLsizei stride, void const * pointer)
		{
			GLint size = Vector::dimension;
			GLenum type = detail::gl_type_v<typename Vector::value_type>;
			return attrib_i(index, size, type, normalized, stride, pointer);
		}

		vertex_array_binder & attrib_i (GLuint index, attribute_description const & desc, void const * pointer = nullptr)
		{
			return attrib_i(index, desc.size, desc.type, desc.normalized, desc.stride, reinterpret_cast<char const *>(pointer) + desc.offset);
		}

		vertex_array_binder & divisor (GLuint index, GLuint divisor)
		{
			gl::VertexAttribDivisor(index, divisor);
			return *this;
		}

		vertex_array_binder & draw (GLenum mode, GLint first, GLsizei count)
		{
			gl::DrawArrays(mode, first, count);
			return *this;
		}

		template <typename T>
		vertex_array_binder & draw_elements (GLenum mode, GLsizei count, std::size_t index_offset)
		{
			gl::DrawElements(mode, count, detail::gl_type_v<T>, static_cast<T const *>(nullptr) + index_offset);
			return *this;
		}

		vertex_array_binder & draw_instanced (GLenum mode, GLint first, GLsizei count, GLsizei instance_count)
		{
			gl::DrawArraysInstanced(mode, first, count, instance_count);
			return *this;
		}

		template <typename T>
		vertex_array_binder & draw_elements_instanced (GLenum mode, GLsizei count, std::size_t index_offset, GLsizei instance_count)
		{
			gl::DrawElementsInstanced(mode, count, detail::gl_type_v<T>, static_cast<T const *>(nullptr) + index_offset, instance_count);
			return *this;
		}
	};

	struct vertex_array
		: detail::object<vertex_array>
	{
		vertex_array ( )
			: object(detail::create_vertex_array_id())
		{ }

		vertex_array (std::nullopt_t)
		{ }

		vertex_array_binder<false> bind ( )
		{
			gl::BindVertexArray(id());
			return {};
		}

		vertex_array_binder<true> bind_scoped ( )
		{
			gl::BindVertexArray(id());
			return {};
		}

	private:
		friend detail::object_access;

		void do_reset ( )
		{
			detail::delete_vertex_array_id(id());
		}
	};


}
