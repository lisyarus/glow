#pragma once

#include <glow/color.hpp>
#include <glow/attribute.hpp>
#include <glow/span.hpp>

#include <geom/alias/vector.hpp>

namespace glow
{
namespace mesh
{

	struct axes
	{
		struct vertex
		{
			geom::vector_4f position;
			glow::color_4f  color;
		};

		static constexpr vertex vertices[6]
		{
			{ { 0.f, 0.f, 0.f, 1.f}, glow::red   },
			{ { 1.f, 0.f, 0.f, 1.f}, glow::red   },
			{ { 0.f, 0.f, 0.f, 1.f}, glow::green },
			{ { 0.f, 1.f, 0.f, 1.f}, glow::green },
			{ { 0.f, 0.f, 0.f, 1.f}, glow::blue  },
			{ { 0.f, 0.f, 1.f, 1.f}, glow::blue  },
		};

		static constexpr auto attrs = glow::interleaved<geom::vector_4f, glow::color_4f>();

		auto data ( ) const
		{
			return glow::span{vertices};
		}

		auto position_attr ( ) const
		{
			return std::get<0>(attrs);
		}

		auto color_attr ( ) const
		{
			return std::get<1>(attrs);
		}

		GLenum mode ( ) const
		{
			return gl::LINES;
		}
	};

}
}
