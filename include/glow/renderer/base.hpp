#pragma once

#include <glow/drawable.hpp>
#include <glow/span.hpp>
#include <glow/object/program.hpp>

namespace glow
{
namespace renderer
{
namespace detail
{

	struct renderer_access
	{
		template <typename Renderer, typename Mesh, typename Binder>
		void setup_vao (Renderer & r, Mesh const & m, Binder && binder)
		{
			r.setup_vao(m, std::forward<Binder>(binder));
		}
	};

	template <typename Derived>
	struct renderer_base
		: renderer_access
	{
		template <typename ... Shaders>
		renderer_base (Shaders && ... shaders)
			: program_ { std::forward<Shaders>(shaders)... }
		{ }

		template <typename Mesh>
		drawable create (Mesh const & m)
		{
			drawable d;
			update(d, m);
			return d;
		}

		template <typename Mesh>
		void update (drawable & d, Mesh const & m)
		{
			auto vertices = m.data();

			d.count = vertices.size();
			d.mode = m.mode();

			if (!d.vao)
				d.vao = glow::vertex_array();

			if (!d.vbo)
				d.vbo = glow::buffer();

			d.vbo.bind_scoped(gl::ARRAY_BUFFER)
				.data(vertices)
				.with([&]{
					setup_vao(derived(), m, d.vao.bind_scoped());
				});
		}

		template <typename Drawable>
		void render (Drawable && d)
		{
			program_.bind_scoped().with([&]{ d.draw(); });
		}

		glow::program & program ( )
		{
			return program_;
		}

	private:
		Derived & derived ( )
		{
			return static_cast<Derived &>(*this);
		}

		glow::program program_;
	};

}
}
}
