#pragma once

#include <glow/renderer/base.hpp>

namespace glow
{
namespace renderer
{

	struct colored
		: detail::renderer_base<colored>
	{
		colored ( );

		void transform (geom::matrix_4x4f const & m);

	private:

		friend struct detail::renderer_access;

		template <typename Mesh, typename Binder>
		void setup_vao (Mesh const & m, Binder && binder)
		{
			binder
				.attrib(0, m.position_attr())
				.attrib(1, m.color_attr())
				;
		}
	};

}
}
