#pragma once

#include <glow/renderer/base.hpp>

namespace glow
{
namespace renderer
{

	struct plain
		: detail::renderer_base<plain>
	{
		plain ( );

		void transform (geom::matrix_4x4f const & m);
		void color (glow::color_4f const & c);

	private:

		friend struct detail::renderer_access;

		template <typename Mesh, typename Binder>
		void setup_vao (Mesh const & m, Binder && binder)
		{
			binder
				.attrib(0, m.position_attr())
				;
		}
	};

}
}
