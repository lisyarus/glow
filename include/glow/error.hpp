#pragma once

#include <glow/gl.hpp>

#include <stdexcept>

namespace glow
{

	namespace detail
	{

		const char * error_enum_to_string (GLenum error);

	}

	struct exception
		: std::runtime_error
	{
		exception (GLenum error, std::string const & message)
			: std::runtime_error(detail::error_enum_to_string(error) + message)
			, err(error)
		{ }

		GLenum error ( ) const
		{
			return err;
		}

	private:
		GLenum err;
	};

	inline GLenum error (std::nothrow_t)
	{
		return gl::GetError();
	}

	inline void error (std::string message = {})
	{
		GLenum e = error(std::nothrow);
		if (e != gl::NO_ERROR_)
			throw exception(e, message);
	}

}
