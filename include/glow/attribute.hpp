#pragma once

#include <glow/gl.hpp>

#include <geom/primitives/vector.hpp>
#include <geom/primitives/point.hpp>

#include <tuple>

namespace glow
{

	struct attribute_description
	{
		GLint size;
		GLenum type;
		GLboolean normalized;
		GLsizei stride;
		GLsizei offset;
	};

	template <typename T>
	struct normalized
	{ };

	namespace detail
	{

		template <typename T>
		struct gl_type;

		template < >
		struct gl_type<GLbyte>
		{
			static constexpr GLenum value = gl::BYTE;
		};

		template < >
		struct gl_type<GLubyte>
		{
			static constexpr GLenum value = gl::UNSIGNED_BYTE;
		};

		template < >
		struct gl_type<GLshort>
		{
			static constexpr GLenum value = gl::SHORT;
		};

		template < >
		struct gl_type<GLushort>
		{
			static constexpr GLenum value = gl::UNSIGNED_SHORT;
		};

		template < >
		struct gl_type<GLint>
		{
			static constexpr GLenum value = gl::INT;
		};

		template < >
		struct gl_type<GLuint>
		{
			static constexpr GLenum value = gl::UNSIGNED_INT;
		};

		template < >
		struct gl_type<GLfloat>
		{
			static constexpr GLenum value = gl::FLOAT;
		};

		template < >
		struct gl_type<GLdouble>
		{
			static constexpr GLenum value = gl::DOUBLE;
		};

		template <typename T>
		constexpr GLenum gl_type_v = gl_type<T>::value;

		template <typename T>
		struct gl_attrib_size
		{
			static constexpr GLint value = 1;
		};

		template <typename T>
		struct gl_attrib_size<normalized<T>>
		{
			static constexpr GLint value = gl_attrib_size<T>::value;
		};

		template <typename T, std::size_t D>
		struct gl_attrib_size<T[D]>
		{
			static constexpr GLint value = static_cast<GLint>(D);
		};

		template <typename T, std::size_t D>
		struct gl_attrib_size<geom::vector<T, D>>
		{
			static constexpr GLint value = static_cast<GLint>(D);
		};

		template <typename T, std::size_t D>
		struct gl_attrib_size<geom::point<T, D>>
		{
			static constexpr GLint value = static_cast<GLint>(D);
		};

		template <typename T>
		constexpr GLint gl_attrib_size_v = gl_attrib_size<T>::value;

		template <typename T>
		struct gl_attrib_type
		{
			using type = T;
		};

		template <typename T>
		struct gl_attrib_type<normalized<T>>
		{
			using type = typename gl_attrib_type<T>::type;
		};

		template <typename T, std::size_t D>
		struct gl_attrib_type<T[D]>
		{
			using type = T;
		};

		template <typename T, std::size_t D>
		struct gl_attrib_type<geom::vector<T, D>>
		{
			using type = T;
		};

		template <typename T, std::size_t D>
		struct gl_attrib_type<geom::point<T, D>>
		{
			using type = T;
		};

		template <typename T>
		using gl_attrib_type_t = typename gl_attrib_type<T>::type;

		template <typename T>
		struct attrib_type
		{
			using type = T;
		};

		template <typename T>
		struct attrib_type<normalized<T>>
		{
			using type = typename attrib_type<T>::type;
		};

		template <typename T>
		using attrib_type_t = typename attrib_type<T>::type;

		template <typename T>
		constexpr GLsizei attrib_size_v = sizeof(attrib_type_t<T>);

		template <typename T>
		struct is_normalized
			: std::integral_constant<GLenum, gl::FALSE_>
		{ };

		template <typename T>
		struct is_normalized<normalized<T>>
			: std::integral_constant<GLenum, gl::TRUE_>
		{ };

		template <typename Desc>
		constexpr auto interleaved_impl (GLsizei stride, GLsizei offset, Desc const & desc)
		{
			return desc;
		}

		template <typename First, typename ... Attribs, typename Desc>
		constexpr auto interleaved_impl (GLsizei stride, GLsizei offset, Desc const & desc)
		{
			attribute_description const current_desc =
			{
				gl_attrib_size_v<First>,
				gl_type_v<gl_attrib_type_t<First>>,
				is_normalized<First>::value,
				stride,
				offset
			};

			return interleaved_impl<Attribs...>(stride, offset + attrib_size_v<First>, std::tuple_cat(desc, std::make_tuple(current_desc)));
		}
	}

	template <typename ... Attribs>
	constexpr auto interleaved ( )
	{
		GLsizei const stride = (detail::attrib_size_v<Attribs> + ... + 0);
		return detail::interleaved_impl<Attribs...>(stride, 0, std::tuple<>{});
	}

}
